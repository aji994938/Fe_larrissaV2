import { defineStore } from "pinia";
import axios from "axios";
import cookie from "vue-cookies";
import { dateFormat, dateNumbers } from "./functions";
import moment from "moment";

// import md5 from "md5";

export const useTaskStore = defineStore("counter", {
  state: () => ({
    //get token and id
    // id: cookie.get("user").id,
    Token: cookie?.get("token"),
    // token2:"",
    Token2: cookie?.get("user")?.token,

    //login state
    Password: "",
    Username: "",

    //data update karyawan
    _username: "",
    _tgllahir: "",
    _jnskelamin: "",
    _email: "",
    _notelp: "",
    _tglpkwt: "",
    _cabang: "",
    _foto: "anony.png",
    _jabatan: "",
    _tglexp: "",
    _status: "",
    kategorikaryawan: "",

    _alamatdom: "",
    _alamatktp: "",
    _provinsidom: "",
    _provinsiktp: "",
    _kabupatendom: "",
    _kabupatenktp: "",
    _kecamatandom: "",
    _kecamatanktp: "",
    _kelurahanktp: "",
    _kelurahandom: "",
    _password: "",
    _sortjabatan: [],

    passbaru: "",

    // state perbantuan karyawan
    cabangperbantuan: "",
    tgl_perbantuan: "",
    dates_perbantuan: [],
    dateExp: "",
    jabatanperbantuan: "",
    fiturperbantuan: "",
    cobaja: [],
    FiturPilihan: [],
    IdFiturPilihan: [],
    tgl_check: [],

    //dashboard state
    _allfiturs: [],
    _fiturs: [],
    _myid: "",

    //karyawan state
    DataUserFitur: [],
    DataWithoutFilter: [],
    DataJab: [],
    DataCab: [],

    // add fitur karyawan state
    _DataFiturSudah: [],
    _DataFiturBelum: [],
    _Nama: "",
    _Nhp: "",
    _NIK: "",
    _Jabatan: "",
    _Cabang: "",
    _fitur: "",

    //produk state
    _DataJenis: [],
    _DataKategori: [],
    _DataProduk: [],
    _DataProdukWithoutFilter: [],
    _DataLength: 1,
    _PhotoFileName: "",
    API: process.env.VUE_APP_API_URL,
    Photo: process.env.VUE_APP_PHOTO_URL,

    // kategori section
    kategori: [],
    _namakategori: "",
    _keterangan: "",

    //aktivitas section
    aktivitasgrup: [],
    aktivitasjabatan: [],
    aktivitas: [],
    nama_aktivitas: "",
    keterangan: "",
    grupselect: "",
    jabatanselect: "",

    //grup section
    grup: [],
    _namagrup: "",

    //cabang section
    allcabang: [],
    kelurahan: [],
    kecamatan: [],
    provinsi: [],
    wilayah: [],
    kabupaten: [],
    kelurahanselect: "",
    kabupatenselect: "",
    wilayahselect: "",
    provinsiselect: "",
    kecamatanselect: "",
    kode_cabang: "",
    nama_cabang: "",
    telepon: "",
    pkp: "",
    nama_resmi: "",
    npwp: "",
    alamat: "",
    target: "",

    //detailkaryawanstate
    DetailPerbantuan: [],
    DetailKaryawan: "",
    namaKaryawan: "",
    idKaryawan: "",
    CabangUtama: "",

    // aset state
    kategoriselect: "",
    cabangselect: "",
    grupasetselect: "",
    nama_aset: "",
    detail_aset: "",
    value_awal: "",
    depresiasi: "",
    value_akhir: "",
    tgl_expired: "",
    tgl_akuisisi: "",
    barcode_aset: "",
    keterangan_aset: "",
    status_aset: "",
    foto_aset: "",
    kategorilist: [],
    cabanglist: [],
    grupaset: [],
    allaset: [],
    allfoto: [],
    id_aktivitasaset: "",
    KelasGrupAset: [],
    KelasPick: "",

    // aktivitas aset state
    id_aset: "",
    id_aktivitas: "",
    exp_date: "",
    foto_aktivitasaset: "",
    status_aktivitas: "",
    keterangan_aktivitas: "",
    aktivitas_asetselect: "",
    aktivitasasetDropDown: [],
    alldetailaset: [],
    lingkupkerja: "",
    kondisilingkup: "",
    keteranganlingkup: "",
    allLingkupKerja: [],
    tgl_tampil: "",

    // task state
    cabangtaskselect: "",
    allAsetCabang: [],
    asettaskselect: "",
    aksettaskselect: "",
    allAkivitasAset: [],
    nama_task: "",
    keterangan_task: "",
    lokasi_aset: "",
    waktu_mulai: "",
    waktu_selesai: "",
    AllTask: [],
    AllLingkuKerjaTask: [],
    LogData: [],
    TaskNotClaim: "",

    // operasional aktivitas aset state
    foto_operasional: [],
    opera: "",
    FotoOperasionals: [],

    //error handling
    usernotfound: "",
    passworderr: "",
    addkaryawanerr: "",
    addkategorierr: "",
    addaktivitaserr: "",
    addcabangerr: "",
    addgruperr: "",
    errormessage: "",
    errormessagetgl: [],

    succes: false,
  }),
  getters: {
    //dashboard
  },
  actions: {
    //multiuse  store
    imgupload(formData) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      axios
        .post(this.API + "api/extra/upload", formData, config)
        .then((response) => {
          this._PhotoFileName = response.data.result[0].image;
          this._foto = response.data.result[0].image;
          this.foto_aset = response.data.result[0].image;
          this.foto_aktivitasaset = response.data.result[0].image;
          this.opera = response.data.result[0].image;
          this.foto_operasional.push(response.data.result[0].image);
        });
    },

    //login page
    loginfunc() {
      const dat = {
        username: this.Username,
        password: this.Password,
      };
      axios
        .post(this.API + "api/extra/getUserToken", dat, {
          headers: { "Content-Type": "multipart/form-data" },
        })
        .then((res) => {
          this.Token = cookie.set("token", res.data.result[0].token, "1d");

          const config = {
            headers: {
              Authorization: "Bearer " + cookie.get("token"),
              "Content-Type": "multipart/form-data",
            },
          };
          axios
            .post(this.API + "api/extra/login", dat, config)
            .then((response) => {
              if (response.data?.result[0].username == dat.username) {
                cookie.set("user", response.data?.result[0], "1d");
                this.Username = "";
                this.Password = "";
                this.Token = cookie?.get("user")?.token;
                this.$router.push({ name: "MainLayout" });
                setTimeout(() => {
                  window.location.reload();
                }, 1);
              } else {
                console.log("KO");
              }
            })
            .catch((err) => {
              this.passworderr = "Password Salah";
              setTimeout(() => {
                this.passworderr = "";
              }, 3000);
            });
        })
        .catch((e) => {
          console.log(e);
          this.passworderr = "User Not Found";
          setTimeout(() => {
            this.passworderr = "";
          }, 3000);
        });
    },

    async refreshdashboard(id, token) {
      const config = {
        headers: { Authorization: "Bearer " + token },
      };

      await axios.get(this.API + "/api/base/fitur", config).then((response) => {
        this._allfiturs = response.data.result;
        // console.log(this._allfiturs);
      });

      axios
        .get(this.API + "api/extra/getFiturSide?id=" + id, config)
        .then((response) => {
          this._fiturs = response.data.result;
        });
      axios
        .get(this.API + "/api/extra/getUser?id=" + id, config)
        .then((response) => {
          this._myid = response.data.result[0].id_jabatan;
          this.Token2 = cookie.get("user").token;
        });
    },

    // fitur page
    async refreshData(token) {
      const config = {
        headers: { Authorization: "Bearer " + token },
      };
      await axios
        .get(this.API + "api/extra/getListKaryawan", config)
        .then((response) => {
          this.DataUserFitur = response.data.result;
          this.DataWithoutFilter = response.data.result;
        });
      // axios
      // .get(this.API + )
    },

    async sortfilter(prop, asc) {
      this.DataUserFitur = this.DataWithoutFilter.sort(function (a, b) {
        if (asc) {
          return a[prop] > b[prop] ? 1 : a[prop] < b[prop] ? -1 : 0;
        } else {
          return b[prop] > a[prop] ? 1 : b[prop] < a[prop] ? -1 : 0;
        }
      });
    },

    // karyawan fitur page

    async refreshDataFitur(id) {
      const config = {
        headers: { Authorization: "Bearer " + this.Token2 },
      };
      await axios
        .get(this.API + "api/extra/getFiturUser?id=" + id, config)
        .then((response) => {
          this._DataFiturSudah = response.data.result;
        });
      axios
        .get(this.API + "api/extra/getAllFiturUser?id=" + id, config)
        .then((response) => {
          this._DataFiturBelum = response.data.result;
        });
      axios.get(this.API + "api/extra/getfitur", config).then((response) => {
        this._allfiturs = response.data.result;
        console.log(this._allfiturs);
      });
      axios
        .get(this.API + "api/extra/getDetailKaryawan?id=" + id, config)
        .then((response) => {
          this._Nama = response.data.result[0].nama;
          this._Nhp = response.data.result[0].hp;
          this._NIK = response.data.result[0].nik;
          this._Jabatan = response.data.result[0].jabatan;
          this._Cabang = response.data.result[0].cabang;
        });
    },

    async addfitur(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      const data = {
        id_fitur: this._fitur,
        id_karyawan: id,
      };

      console.log(data);
      try {
        const res = await axios.post(
          this.API + "/api/base/hakakses",
          data,
          config
        );

        if (res.status === 200) {
          this.succes = true;
          this._fitur = "";
        }
      } catch (error) {
        console.log(error);
      }
    },

    async deletefitur(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
        },
      };

      try {
        const res = await axios.delete(
          this.API + '/api/base/hakakses?where={"id":"=' + id + '"}',
          config
        );
        console.log(res);
        if (res.status == 200) {
          console.log("Success Deleted");
        }
      } catch (error) {
        console.log(error);
      }
    },

    //func produk
    async refreshdataproduk(toktok, idCabang) {
      const config = {
        headers: { Authorization: "Bearer " + toktok },
      };

      axios
        .get(this.API + "api/extra/getProduk?id=" + idCabang, config)
        .then((response) => {
          this._DataProduk = response.data.result;
          this._DataWithoutFilter = response.data.result;
          this._DataLength = this._DataProduk.length;
        });
      axios
        .get(this.API + "api/extra/getListJenisProduk", config)
        .then((response) => {
          this._DataJenis = response.data.result;
          //   console.log(this.DataJenis)
        });

      axios
        .get(this.API + "api/extra/getListKategoriProduk", config)
        .then((response) => {
          this._DataKategori = response.data.result;
          //   console.log(this.DataKategori)
        });
    },

    async addproduk(token, data) {
      const config = {
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "multipart/form-data",
        },
      };
      await axios
        .post(this.API + "/api/base/produk", data, config)
        .then((response) => {
          console.log(response.data.result);

          if (response.data.result[0].message == "Data Successfully Added") {
            console.log("OK");
          } else {
            console.log("KO");
          }
        });
    },

    updateproduk(toktok, idProduk, data) {
      const config = {
        headers: {
          Authorization: "Bearer " + toktok,
          "Content-Type": "multipart/form-data",
        },
      };
      axios
        .put(
          this.API + '/api/base/produk?where={"id":"=' + idProduk + '"}',
          data,
          config
        )
        .then((response) => {
          console.log(response);
        });
    },

    async deleteproduk(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token,
          "Content-Type": "multipart/form-data",
        },
      };
      await axios
        .delete(
          this.API + '/api/base/produk?where={"id":"=' + id + '"}',
          config
        )
        .then((response) => {
          console.log(response);
        });
    },

    filterproduk(data) {
      this._DataProduk = this._DataWithoutFilter.filter(function (el) {
        return (
          el.nama
            .toString()
            .toLowerCase()
            .includes(data.NamaFilter.toString().trim().toLowerCase()) &&
          el.barcode
            .toString()
            .toLowerCase()
            .includes(data.SKUFilter.toString().trim().toLowerCase()) &&
          el.jenis
            .toString()
            .toLowerCase()
            .includes(data.JenisFilter.toString().trim().toLowerCase()) &&
          el.kategori
            .toString()
            .toLowerCase()
            .includes(data.KategoriFilter.toString().trim().toLowerCase()) &&
          el.keterangan
            .toString()
            .toLowerCase()
            .includes(data.KeteranganFilter.toString().trim().toLowerCase())
        );
      });
    },

    sortproduk(prop, asc) {
      this._DataProduk = this._DataWithoutFilter.sort(function (a, b) {
        if (asc) {
          return a[prop] > b[prop] ? 1 : a[prop] < b[prop] ? -1 : 0;
        } else {
          return b[prop] > a[prop] ? 1 : b[prop] < a[prop] ? -1 : 0;
        }
      });
    },

    //add karyawan

    refreshaddkaryawan() {
      this._username = "";
      this._password = "";
      this._nama = "";
      this._email = "";
      this._hp = "";
      this._nik = "";
      this._tgl_masuk = "";
      this._alamat = "";
      this._foto = "";
      this._jabatan = "";
      this._cabang = "";
    },

    clearStateKaryawan() {
      this._username = "";
      this._tgllahir = "";
      this._jnskelamin = "";
      this._email = "";
      this._notelp = "";
      this._nik = "";
      this._tglpkwt = "";
      this._foto = "";
      this._jabatan = "";
      this._cabang = "";
      this._tglexp = "";
      this._status = "";

      this._alamatdom = "";
      this._alamatktp = "";
      this._provinsiktp = "";
      this._kabupatenktp = "";
      this._kecamatanktp = "";
      this._kelurahanktp = "";
      this._provinsidom = "";
      this._kabupatendom = "";
      this._kecamatandom = "";
      this._kelurahandom = "";
      (this.kategorikaryawan = ""), (this._passwor = "");
      this.passbaru = "";
    },

    async addkaryawan() {
      const data = {
        username: this._username,
        tgllahir: dateFormat(this._tgllahir),
        jenis_kelamin: this._jnskelamin,
        email: this._email,
        hp: this._notelp,
        nik: this._nik,
        tgl_buat: dateFormat(this._tglpkwt),
        foto: this._foto,
        id_jabatan: this._jabatan,
        id_cabang: this._cabang,
        id_fitur: this._fitur,
        tgl_exp: dateFormat(this._tglexp),
        status: this._status === "Aktif" ? 1 : 2,
        kategorikaryawan: this.kategorikaryawan,

        alamat_dom: this._alamatdom,
        alamat_ktp: this._alamatktp,
        id_provinsi_ktp: this._provinsiktp,
        id_kabupaten_ktp: this._kabupatenktp,
        id_kecamatan_ktp: this._kecamatanktp,
        id_kelurahan_ktp: this._kelurahanktp,
        id_provinsi_dom: this._provinsidom,
        id_kabupaten_dom: this._kabupatendom,
        id_kecamatan_dom: this._kecamatandom,
        id_kelurahan_dom: this._kelurahandom,
        password_baru: this._password,
      };

      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      try {
        const response = await axios.post(
          this.API + "api/extra/addKaryawan",
          data,
          config
        );
        console.log(response);
        if (response.status === 200) {
          this.succes = true;
        }
      } catch (err) {
        this.succes = false;
        this.addkaryawanerr = err?.response?.data;
      }
    },

    async updateDataKaryawan(id) {
      function dateFormat(date) {
        date = new Date(date);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        return new Date([year, month, day].join("-"));
      }
      console.log(this._tgllahir);
      console.log(dateFormat(this._tgllahir));

      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };
      const data = {
        username: this._username,
        tgllahir: dateFormat(this._tgllahir),
        jenis_kelamin: this._jnskelamin,
        email: this._email,
        hp: this._notelp,
        nik: this._nik,
        tgl_buat: dateFormat(this._tglpkwt),
        foto: this._foto,
        id_cabang: this._cabang,
        tgl_exp: dateFormat(this._tglexp),
        status: this._status === "Aktif" ? 1 : 2,

        alamat_dom: this._alamatdom,
        alamat_ktp: this._alamatktp,
        id_provinsi_ktp: this._provinsiktp,
        id_kabupaten_ktp: this._kabupatenktp,
        id_kecamatan_ktp: this._kecamatanktp,
        id_kelurahan_ktp: this._kelurahanktp,
        id_provinsi_dom: this._provinsidom,
        id_kabupaten_dom: this._kabupatendom,
        id_kecamatan_dom: this._kecamatandom,
        id_kelurahan_dom: this._kelurahandom,
      };

      // if (!data.tgl_masuk) {
      //   this.succes === false
      //   this.addkaryawanerr = "Tanggal Belum Dimasukkan";

      // } else if (!data.id_cabang) {
      //   this.succes === false
      //   this.addkaryawanerr = "Cabang Belum Dimasukkan";
      // }
      // else if (!this._jabatan) {
      //   this.succes === false
      //   this.addkaryawanerr = "Jabatan Belum Dimasukkan";
      // }
      // else if (!data.password_baru) {
      //   this.succes === false
      //   this.addkaryawanerr = "Password Belum Dimasukkan";
      // }
      // else {
      try {
        if (this.passbaru) {
          axios.put(
            this.API + "/api/extra/editpasword?id=" + id,
            { password_baru: this.passbaru },
            config
          );
        }
        axios.put(
          this.API +
            '/api/base/jabatan_utama?where={"id_karyawan":"=' +
            id +
            '"}',
          { id_jabatan: this._jabatan },
          config
        );

        const response = await axios.put(
          this.API + '/api/base/karyawan?where={"id":"=' + id + '"}',
          data,
          config
        );
        if (response.status === 200) {
          this.succes = true;
        }
      } catch (err) {
        this.succes = false;
        this.addkaryawanerr = err?.response?.data;
      }
      // }
    },

    changestatuskaryawan(id, status, Token) {
      const config = {
        headers: {
          Authorization: "Bearer " + Token,
          "Content-Type": "multipart/form-data",
        },
      };

      axios
        .put(
          this.API + '/api/base/karyawan?where={"id":"=' + id + '"}',
          { status: status },
          config
        )
        .then((response) => {
          if (response.data.status === 200) {
            console.log("Status Berhasil Diubah");
          } else {
            console.log("KO");
          }
        });
    },

    getdatakaryawan(id, token) {
      const config = {
        headers: { Authorization: "Bearer " + token },
      };

      this._jabatan = "";
      axios
        .get(
          `${this.API}/api/base/jabatan_utama?where={"id_karyawan":"='${id}'"}`,
          config
        )
        .then((res) => {
          this._jabatan = res.data.result[0].id_jabatan;
          console.log(res.data.result[0]);
        });

      axios
        .get(this.API + '/api/base/karyawan?where={"id":"=' + id + '"}', config)
        .then((response) => {
          if (response.data.status === 200) {
            // console.log(da);
            const data = response.data.result[0];
            this._username = data.username;
            this._tgllahir = dateFormat(data.tgllahir);
            this._jnskelamin = data.jenis_kelamin;
            this._email = data.email;
            this._notelp = data.hp;
            this._nik = data.nik;
            this._tglpkwt = dateFormat(data.tgl_buat);
            this._foto = data.foto;
            this._cabang = data.id_cabang;
            this._tglexp = dateFormat(data.tgl_exp);
            this._status = data.status === 1 ? "Aktif" : "Nonaktif";
            this._alamatdom = data.alamat_dom;
            this._alamatktp = data.alamat_ktp;
            this._provinsiktp = data.id_provinsi_ktp;
            this._kabupatenktp = data.id_kabupaten_ktp;
            this._kecamatanktp = data.id_kecamatan_ktp;
            this._kelurahanktp = data.id_kelurahan_ktp;
            this._provinsidom = data.id_provinsi_dom;
            this._kabupatendom = data.id_kabupaten_dom;
            this._kecamatandom = data.id_kecamatan_dom;
            this._kelurahandom = data.id_kelurahan_dom;
            this.kategorikaryawan = data.kategorikaryawan;
            this._password = data.password_baru;
          } else {
            console.log("KO");
          }
        });
    },

    async deleteJabatanPerbantuan(id) {
      const config = {
        headers: { Authorization: "Bearer " + this.Token2 },
      };
      console.log(id);
      try {
        const res = await axios.delete(
          this.API + "/api/extra/deleteuserperbantuan?id=" + id,
          config
        );
      } catch (error) {
        console.log(error);
      }
    },

    getdetailkaryawan(id) {
      const config = {
        headers: { Authorization: "Bearer " + this.Token2 },
      };

      axios
        .get(this.API + "/api/extra/getdetailusertemp?id=" + id, config)
        .then((res) => {
          if (res.status === 200) {
            this.DetailPerbantuan = res.data.result;
          }
        })
        .catch((this.errormessage = "Karyawan Not Found"));

      axios
        .get(this.API + "/api/extra/getDetailKaryawan?id=" + id, config)
        .then((res) => {
          if (res.status === 200) {
            const data = res.data.result[0];
            this.namaKaryawan = data.username;
            this.idKaryawan = data.id;
            this.CabangUtama = data.cabang;
          }
        })
        .catch((this.errormessage = "Karyawan Not Found"));
    },

    async addfiturtemp(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      const data = {
        id_fitur: this.fiturperbantuan,
        id_karyawan: id,
      };

      try {
        const res = await axios.post(
          this.API + "/api/base/hakakses_temp",
          data,
          config
        );

        if (res.status === 200) {
          this.succes = true;
          (this.fiturperbantuan = ""), console.log(this.succes);
        }
      } catch (error) {
        console.log(error);
      }
    },

    // kategori section
    emptykategori() {
      this._namakategori = "";
      this._keterangan = "";
    },

    getkategori() {
      let config = {
        headers: { Authorization: "Bearer " + this?.Token2 },
      };

      axios.get(this.API + "api/base/kategori", config).then((res) => {
        if (res.data.status === 200) {
          this.kategori = res.data.result;
        } else {
          console.log("Terjadi Something");
        }
      });
    },

    async addkategori() {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token,
          "Content-Type": "multipart/form-data",
        },
      };

      let data = {
        nama_kategori: this._namakategori,
        keterangan: this._keterangan,
      };

      if (data.nama_kategori === "") {
        this.succes = false;
        this.addkategorierr = "Nama Kategori Belum Dimasukkan";
      } else if (data.keterangan === "") {
        this.succes = false;
        this.addkategorierr = "Keterangan Belum Dimasukkan";
      } else {
        try {
          const res = await axios.post(
            `${this.API}api/base/kategori`,
            data,
            config
          );
          if (res.data.status === 200) {
            this.succes = true;
            this.addkategorierr = "";
            this._namakategori = "";
            this._keterangan = "";
          }
        } catch (err) {
          this.succes = false;
        }
      }
    },

    deletekategori(id) {
      const config = {
        headers: { Authorization: "Bearer " + this.Token2 },
      };
      axios
        .delete(`${this.API}api/base/kategori?where={"id":"='${id}'"}`, config)
        .then((res) => {
          if (res.data.status === 200) {
            console.log("delete kategori");
          } else {
            console.log("Terjadi Something");
          }
        });
    },

    getonekategori(id) {
      const config = {
        headers: { Authorization: "Bearer " + this.Token },
      };

      axios
        .get(this.API + '/api/base/kategori?where={"id":"=' + id + '"}', config)
        .then((response) => {
          if (response.data.status === 200) {
            // console.log(da);
            const data = response.data.result[0];
            this._namakategori = data.nama_kategori;
            this._keterangan = data.keterangan;
          } else {
            console.log("KO");
          }
        });
    },

    async editkategori(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token,
          "Content-Type": "multipart/form-data",
        },
      };
      let data = {
        nama_kategori: this._namakategori,
        keterangan: this._keterangan,
      };
      if (data.nama_kategori === "") {
        this.succes = false;
        this.addkategorierr = "Nama Kategori Belum Dimasukkan";
      } else if (data.keterangan === "") {
        this.succes = false;
        this.addkategorierr = "Keterangan Belum Dimasukkan";
      } else {
        try {
          const res = await axios.put(
            `${this.API}api/base/kategori?where={"id":"='${id}'"}`,
            data,
            config
          );
          if (res.data.status === 200) {
            this.succes = true;
            this.addkategorierr = "";
            this._namakategori = "";
            this._keterangan = "";
          }
        } catch (err) {
          this.succes = false;
        }
      }
    },

    //aktivitas section
    emptyaktivitas() {
      (this.nama_aktivitas = ""),
        (this.keterangan = ""),
        (this.grupselect = "");
      this.jabatanselect = "";
      this.artivi;
    },

    async getgrupinaktivitas() {
      const config = {
        headers: { Authorization: "Bearer " + this?.Token },
      };
      try {
        const resjabatan = await axios.get(
          this.API + "api/extra/getDropJabatan",
          config
        );
        const resgrup = await axios.get(
          `${this.API}api/extra/gruptext`,
          config
        );

        this.aktivitasgrup = resgrup.data.result;
        this.aktivitasjabatan = resjabatan.data.result;
      } catch (error) {
        console.log(error);
      }
    },

    getoneaktivitas(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token,
        },
      };
      axios
        .get(`${this.API}api/base/aktivitas?where={"id":"='${id}'"}`, config)
        .then((res) => {
          if (res.data.status === 200) {
            const data = res.data.result[0];
            this.nama_aktivitas = data.nama_aktivitas;
            this.keterangan = data.keterangan;
            this.grupselect = data.id_grup;
            this.jabatanselect = data.id_jabatan;
          } else {
            console.log("Terjadi Something");
          }
        });
    },

    getaktivitas() {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
        },
      };
      axios.get(`${this.API}api/extra/getlistaktivitas`, config).then((res) => {
        if (res.data.status === 200) {
          this.aktivitas = res.data.result;
          console.log(this.aktivitas);
        } else {
          console.log("Terjadi Something");
        }
      });
    },

    async addaktivitas() {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };
      let data = {
        nama_aktivitas: this.nama_aktivitas,
        keterangan: this.keterangan,
        id_grup: this.grupselect,
        id_jabatan: this.jabatanselect,
      };

      if (data.nama_aktivitas === "") {
        this.succes = false;
        this.addaktivitaserr = "Nama Aktivitas Belum Dimasukkan";
      } else if (data.id_grup === "") {
        this.succes = false;
        this.addaktivitaserr = "Grup Belum Ditentukan";
      } else if (data.id_jabatan === "") {
        this.succes = false;
        this.addaktivitaserr = "Jabatan Belum Ditentukan";
      } else {
        try {
          const res = await axios.post(
            `${this.API}api/base/aktivitas`,
            data,
            config
          );
          if (res.data.status === 200) {
            this.succes = true;
            this.addaktivitaserr = "";
            this.$router.push("/aktivitas");
          } else {
            console.log("Terjadi Something");
          }
        } catch (err) {
          this.succes = false;
        }
      }
    },

    deleteaktivitas(id) {
      const config = {
        headers: { Authorization: "Bearer " + this.Token2 },
      };

      axios
        .delete(`${this.API}api/base/aktivitas?where={"id":"='${id}'"}`, config)
        .then((res) => {
          if (res.data.status === 200) {
            console.log("deleteed aktivitas");
          } else {
            console.log("Terjadi Something");
          }
        });
    },

    async updateaktivitas(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };
      let data = {
        nama_aktivitas: this.nama_aktivitas,
        keterangan: this.keterangan,
        id_grup: this.grupselect,
        id_jabatan: this.jabatanselect,
      };

      if (data.nama_aktivitas === "") {
        this.succes = false;
        this.addaktivitaserr = "Nama Aktivitas Belum Dimasukkan";
      } else if (data.id_grup === "") {
        this.succes = false;
        this.addaktivitaserr = "Grup Belum Ditentukan";
      } else if (data.id_jabatan === "") {
        this.succes = false;
        this.addaktivitaserr = "Jabatan Belum Ditentukan";
      } else {
        try {
          const res = await axios.put(
            this.API + '/api/base/aktivitas?where={"id":"=' + id + '"}',
            data,
            config
          );
          if (res.data.status === 200) {
            this.succes = true;
            this.addaktivitaserr = "";
            this.$router.push("/aktivitas");
          } else {
            console.log("Terjadi Something");
          }
        } catch (err) {
          this.succes = false;
        }
      }
    },

    //grup section
    emptygrup() {
      this._namagrup = "";
      this.KelasPick = "";
      this.addgruperr = "";
      this.succes = false
    },

    getgrup() {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
        },
      };
      axios.get(`${this.API}api/base/grup_aset`, config).then((res) => {
        if (res.data.status === 200) {
          this.grup = res.data.result;
        } else {
          console.log("Terjadi Something");
        }
      });
    },

    async addgrup() {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token,
          "Content-Type": "multipart/form-data",
        },
      };

      let data = {
        id_kelas: this.KelasPick,
        name_grupaset: this._namagrup,
      };
      if (!this._namagrup) {
        this.addgruperr = "Input Nama Grup Dulu";
      } else if (!this.KelasPick) {
        this.addgruperr = "Kelas Belum Diinput";
      } else {
        try {
          const res = await axios.post(
            `${this.API}api/base/grup_aset`,
            data,
            config
          );
          if (res.status === 200) {
            this.succes = true;
          }
        } catch (error) {
          this.succes = false;
          console.log(error);
        }
      }
    },

    getonegrup(id) {
      const config = {
        headers: { Authorization: "Bearer " + this.Token },
      };

      axios
        .get(
          this.API + '/api/base/grup_aset?where={"id":"=' + id + '"}',
          config
        )
        .then((response) => {
          if (response.data.status === 200) {
            // console.log(da);
            const data = response.data.result[0];
            this.KelasPick = data.id_kelas;
            this._namagrup = data.name_grupaset;
          } else {
            console.log("KO");
          }
        });
    },

    async updategrup(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };
      let data = {
        id_kelas: this.KelasPick,
        name_grupaset: this._namagrup,
      };
      if (!this._namagrup) {
        this.addgruperr = "Input Nama Grup Dulu";
      } else if (!this.KelasPick) {
        this.addgruperr = "Kelas Grup Belum Diinput";
      } else {
        try {
          const res = await axios.put(
            this.API + '/api/base/grup_aset?where={"id":"=' + id + '"}',
            data,
            config
          );
          if (res.data.status === 200) {
            this.succes = true;
            this._namagrup = "";
            this.KelasPick = "";
           
          } else {
            console.log("KO");
          }
        } catch (error) {
          this.succes = false;
          console.log(error);
        }
      }
    },

    deletegrup(id) {
      const config = {
        headers: { Authorization: "Bearer " + this.Token },
      };
      axios
        .delete(`${this.API}api/base/grup_aset?where={"id":"='${id}'"}`, config)
        .then((res) => {
          if (res.data.status === 200) {
            console.log("delete grup");
          } else {
            console.log("Terjadi Something");
          }
        });
    },

    // get text section
    getkategoritext() {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
        },
      };
      axios
        .get(this.API + "api/extra/getListKategoriProduk", config)
        .then((response) => {
          this._DataKategori = response.data.result;
          this.kategorilist = response.data.result;
          //   console.log(this.DataKategori)
        });
    },

    getcabangtext() {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
        },
      };
      axios
        .get(this.API + "api/extra/getDropCabang", config)
        .then((response) => {
          this.cabanglist = response.data.result;
        });
    },

    getgrupasettext() {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
        },
      };
      axios.get(this.API + "api/extra/getgrupaset", config).then((response) => {
        this.grupaset = response.data.result;
        console.log(this.grupaset);
      });
    },

    // aset section

    emptyStateAset() {
      (this.kategoriselect = ""),
        (this.nama_aset = ""),
        (this.value_awal = ""),
        (this.depresiasi = ""),
        (this.value_akhir = ""),
        (this.tgl_expired = ""),
        (this.tgl_akuisisi = ""),
        (this.status_aset = ""),
        (this.cabangselect = ""),
        (this.barcode_aset = ""),
        (this.keterangan = ""),
        (this.grupasetselect = ""),
        (this.lokasi = ""),
        (this.foto_aset = "");
    },

    async getallaset() {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
        },
      };
      try {
        const res = await axios.get(this.API + "api/extra/getallaset", config);
        this.allaset = res.data.result;
      } catch (error) {
        console.log(error);
      }
    },

    async addaset() {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      let data = {
        id_kategori: this.kategoriselect,
        nama_awal: this.nama_aset,
        value_awal: this.value_awal,
        depresiasi: this.depresiasi,
        value_akhir: this.value_awal - this.depresiasi,
        // tgl_expired: dateNumbers(this.tgl_expired),
        tgl_expired: dateNumbers("2021-10-10"),
        tgl_akuisisi: dateNumbers(this.tgl_akuisisi),
        status_aset: this.status_aset === "Aktif" ? 1 : 2,
        id_cabang: this.cabangselect,
        barcode: this.barcode_aset,
        keterangan: this.keterangan,
        id_grupaset: this.grupasetselect,
        lokasi: this.lokasi,
        image: this.foto_aset,
      };
      console.log(data);

      try {
        const res = await axios.post(
          this.API + "api/extra/addaset",
          data,
          config
        );
        if (res.status === 200) {
          this.succes = true;
        }
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    async getoneaset(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
        },
      };

      try {
        const res = await axios.get(
          this.API + 'api/base/aset?where={"id":"=' + id + '"}',
          config
        );
        const res2 = await axios.get(
          this.API + 'api/base/detail_aset?where={"id_aset":"=' + id + '"}',
          config
        );
       
        const data = res.data.result[0];
        this.id_grupaset = data.id_grupaset;
        this.id_aset = data.id;
        this.kategoriselect = data.id_kategori;
        this.nama_aset = data.nama_awal;
        this.value_awal = data.value_awal;
        this.depresiasi = data.depresiasi;
        this.value_akhir = data.value_akhir;
        this.tgl_akuisisi = dateFormat(data.tgl_akuisisi);
        this.tgl_expired = dateFormat(data.tgl_expired);
        this.status_aset = data.status_aset == 1 ? "Aktif" : "Non Aktif";
        this.cabangselect = data.id_cabang;
        this.barcode_aset = data.barcode;
        this.keterangan = data.keterangan;
        this.grupasetselect = data.id_grupaset;
        this.lokasi = data.lokasi;
        this.allfoto = res2.data.result;
        this.foto_aset = res2.data.result[0].foto;
        
        const res3 = await axios.get(
          this.API + 'api/base/grup_aset?where={"id":"=' + this.id_grupaset + '"}',
          config
        )
        this.KelasPick = res3.data.result[0].id_kelas
      

      } catch (error) {
        console.log(error);
      }
    },

    async editaset(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      let data = {
        id_kategori: this.kategoriselect,
        nama_awal: this.nama_aset,
        value_awal: this.value_awal,
        depresiasi: this.depresiasi,
        value_akhir: this.value_awal - this.depresiasi,
        tgl_akuisisi: dateFormat(this.tgl_akuisisi),
        tgl_expired: dateFormat(this.tgl_expired),
        status_aset: this.status_aset === "Aktif" ? 1 : 2,
        id_cabang: this.cabangselect,
        barcode: this.barcode_aset,
        keterangan: this.keterangan,
        id_grupaset: this.grupasetselect,
        lokasi: this.lokasi,
        image: this.foto_aset,
      };

      try {
        const res = await axios.put(
          this.API + 'api/base/aset?where={"id":"=' + id + '"}',
          data,
          config
        );
        if (res.status == 200) {
          this.succes = true;
        }
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    async addFotoAset(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };
      let data = {
        id_aset: id,
        foto: this.foto_aset,
      };

      try {
        const res = await axios.post(
          this.API + "api/base/detail_aset",
          data,
          config
        );
        if (res.status == 200) {
          this.succes = true;
          this.foto_aset = "";
        }
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    async deleteaset(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
        },
      };

      try {
        const res = await axios.delete(
          this.API + 'api/base/aset?where={"id":"=' + id + '"}',
          config
        );
        if (res.status === 200) {
          this.succes = true;
        }
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    getKelasAset() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      axios.get(this.API + "/api/extra/kelasgruptext", config).then((res) => {
        this.KelasGrupAset = res.data.result;
      });
    },

    // cabang section
    emptycabang() {
      this.kelurahanselect = "";
      this.kecamatanselect = "";
      this.provinsiselect = "";
      this.kabupatenselect = "";
      this.wilayahselect = "";
      this.kode_cabang = "";
      this.nama_cabang = "";
      this.telepon = "";
      (this.pkp = false), (this.nama_resmi = "");
      this.npwp = "";
      this.alamat = "";
      this.target = "";
      this.addcabangerr = "";
    },

    getallselect() {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
        },
      };
      axios
        .get(this.API + "api/extra/getkelurahan", config)
        .then((response) => {
          this.kelurahan = response.data.result;
        });

      axios
        .get(this.API + "api/extra/getkecamatan", config)
        .then((response) => {
          this.kecamatan = response.data.result;
        });

      axios.get(this.API + "api/extra/getwilayah", config).then((response) => {
        this.wilayah = response.data.result;
      });

      axios
        .get(this.API + "api/extra/getkabupaten", config)
        .then((response) => {
          this.kabupaten = response.data.result;
        });

      axios.get(this.API + "api/extra/getprovinsi", config).then((response) => {
        this.provinsi = response.data.result;
      });

      axios
        .get(this.API + "api/extra/getDropJabatan", config)
        .then((response) => {
          this.DataJab = response.data.result;
        });

      axios
        .get(this.API + "api/extra/getDropCabang", config)
        .then((response) => {
          this.DataCab = response.data.result;
        });
    },

    addcabang() {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      let data = {
        id_kelurahan: this.kelurahanselect,
        id_kecamatan: this.kecamatanselect,
        id_provinsi: this.provinsiselect,
        id_kabupaten: this.kabupatenselect,
        id_wilayah: this.wilayahselect,
        kode_cabang: this.kode_cabang,
        nama_cabang: this.nama_cabang,
        telepon: this.telepon,
        pkp: this.pkp === true ? 1 : 2,
        nama_resmi: this.nama_resmi,
        npwp: this.npwp,
        alamat: this.alamat,
        target: !this.target ? 0 : this.target,
      };

      if (!data.id_kelurahan) {
        this.addcabangerr = "Kelurahan Belum di Inputakn";
      } else if (!data.alamat) {
        this.addcabangerr = "Alamat Belum di Inputakn";
      } else if (!data.npwp) {
        this.addcabangerr = "NPWP Belum di Inputakn";
      } else if (!data.telepon) {
        this.addcabangerr = "Telepon Belum di Inputakn";
      } else if (!data.id_kabupaten) {
        this.addcabangerr = "Kabupaten Belum di Inputakn";
      } else if (!data.id_kecamatan) {
        this.addcabangerr = "Kecamatan Belum di Inputakn";
      } else if (!data.id_provinsi) {
        this.addcabangerr = "Provinsi Belum di Inputakn";
      } else if (!data.id_wilayah) {
        this.addcabangerr = "Wilayah Belum di Inputakn";
      } else if (!data.kode_cabang) {
        this.addcabangerr = "Kode Cabang Belum di Inputakn";
      } else if (!data.nama_cabang) {
        this.addcabangerr = "Nama Cabang Belum di Inputakn";
      } else if (!data.nama_resmi) {
        this.addcabangerr = "Nama Resmi Belum di Inputakn";
      } else {
        axios
          .post(this.API + "api/base/cabang", data, config)
          .then((response) => {
            if (response.data.status === 200) {
              this.addcabangerr = "";
              this.$router.push("/cabang");
            } else {
              console.log("Terjadi Something");
            }
          });
      }
    },

    async getfiturperbantuan(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };

      axios
        .get(this.API + "api/extra/getAllFiturUserTemp?id=" + id, config)
        .then((response) => {
          this._DataFiturBelum = response.data.result;
        });

      axios.get(this.API + "api/extra/getfitur", config).then((response) => {
        this._allfiturs = response.data.result;
      });

      await axios
        .get(this.API + "api/extra/getFiturUserTemp?id=" + id, config)
        .then((response) => {
          this._DataFiturSudah = response.data.result;
        });
    },

    async deletefiturTemp(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      try {
        const res = await axios.delete(
          this.API + '/api/base/hakakses_temp?where={"id":"=' + id + '"}',
          config
        );
        if (res.status == 200) {
          console.log("sdfa");
        }
      } catch (error) {
        console.log(error);
      }
    },

    checkPerbantuan(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      axios
        .get(
          this.API + 'api/base/user_temp?where={"id_karyawan":"=' + id + '"}',
          config
        )
        .then((response) => {
          if (response.data.status === 200) {
            this.tgl_check = response.data.result;
          } else {
            console.log("Terjadi Something");
          }
        });
    },

    emptyPerbantuan() {
      this.cabangperbantuan = "";
      this.tgl_perbantuan = "";
      this.jabatanperbantuan = "";
      this.FiturPilihan = [];
      this.IdFiturPilihan = [];
      this.errormessagetgl = "";
    },

    async addperbantuan(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this.Token2,
          "Content-Type": "multipart/form-data",
        },
      };
      let data = {
        id_karyawan: id,
        id_cabang_baru: this.cabangperbantuan,
        jabatan_temp: this.jabatanperbantuan,
        tgl_perbantuan: JSON.stringify(
          this.dates_perbantuan.map((d) => dateNumbers(d))
        ),
        FiturPilihan: JSON.stringify(this.IdFiturPilihan),
      };
      try {
        const res = await axios.post(
          this.API + "api/extra/addperbantuan",
          data,
          config
        );
        if (res.status === 200) {
          this.succes = true;
          this.cabangperbantuan = "";
          this.jabatanperbantuan = "";
          this.dates_perbantuan = [];
          this.dateExp = "";
          this.errormessagetgl = "";
        }
      } catch (error) {
        this.succes = false;
        this.errormessagetgl = error.response.data.error;
      }
    },

    getallperbantuanselect() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      axios
        .get(this.API + "api/extra/getDropCabang", config)
        .then((response) => {
          if (response.data.status === 200) {
            this.DataCab = response.data.result;
          } else {
            console.log("Terjadi Something");
          }
        });

      axios
        .get(this.API + "api/extra/getDropJabatan", config)
        .then((response) => {
          this.DataJab = response.data.result;
        });
    },

    getallcabang() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      axios.get(this.API + "/api/base/cabang", config).then((response) => {
        if (response.data.status === 200) {
          this.allcabang = response.data.result;
        } else {
          console.log("Terjadi Something");
        }
      });
    },

    deletecabang(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      axios
        .delete(`${this.API}api/base/cabang?where={"id":"='${id}'"}`, config)
        .then((res) => {
          if (res.data.status === 200) {
            console.log("delete cabang");
          } else {
            console.log("Terjadi Something");
          }
        });
    },

    getonecabang(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };

      axios
        .get(`${this.API}api/base/cabang?where={"id":"='${id}'"}`, config)
        .then((res) => {
          if (res.data.status === 200) {
            const data = res.data.result[0];
            this.kelurahanselect = data.id_kelurahan;
            this.kecamatanselect = data.id_kecamatan;
            this.provinsiselect = data.id_provinsi;
            this.kabupatenselect = data.id_kabupaten;
            this.wilayahselect = data.id_wilayah;
            this.kode_cabang = data.kode_cabang;
            this.nama_cabang = data.nama_cabang;
            this.telepon = data.telepon;
            (this.pkp = data.pkp === 1 ? true : false),
              (this.nama_resmi = data.nama_resmi);
            this.npwp = data.npwp;
            this.alamat = data.alamat;
            this.target = data.target;
          } else {
            console.log("Terjadi Something");
          }
        });
    },

    async updatecabang(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
          "Content-Type": "multipart/form-data",
        },
      };
      let data = {
        id_kelurahan: this.kelurahanselect,
        id_kecamatan: this.kecamatanselect,
        id_provinsi: this.provinsiselect,
        id_kabupaten: this.kabupatenselect,
        id_wilayah: this.wilayahselect,
        kode_cabang: this.kode_cabang,
        nama_cabang: this.nama_cabang,
        telepon: this.telepon,
        pkp: this.pkp === true ? 1 : 2,
        nama_resmi: this.nama_resmi,
        npwp: this.npwp,
        alamat: this.alamat,
        target: this.target,
      };

      if (!data.id_kelurahan) {
        this.succes = false;
        this.addcabangerr = "Kelurahan Belum di Inputakn";
      } else if (!data.alamat) {
        this.succes = false;
        this.addcabangerr = "Alamat Belum di Inputakn";
      } else if (!data.npwp) {
        this.succes = false;
        this.addcabangerr = "NPWP Belum di Inputakn";
      } else if (!data.telepon) {
        this.succes = false;
        this.addcabangerr = "Telepon Belum di Inputakn";
      } else if (!data.id_kabupaten) {
        this.succes = false;
        this.addcabangerr = "Kabupaten Belum di Inputakn";
      } else if (!data.id_kecamatan) {
        this.succes = false;
        this.addcabangerr = "Kecamatan Belum di Inputakn";
      } else if (!data.id_provinsi) {
        this.succes = false;
        this.addcabangerr = "Provinsi Belum di Inputakn";
      } else if (!data.id_wilayah) {
        this.succes = false;
        this.addcabangerr = "Wilayah Belum di Inputakn";
      } else if (!data.kode_cabang) {
        this.succes = false;
        this.addcabangerr = "Kode Cabang Belum di Inputakn";
      } else if (!data.nama_cabang) {
        this.succes = false;
        this.addcabangerr = "Nama Cabang Belum di Inputakn";
      } else if (!data.nama_resmi) {
        this.succes = false;
        this.addcabangerr = "Nama Resmi Belum di Inputakn";
      } else {
        try {
          const res = await axios.put(
            this.API + '/api/base/cabang?where={"id":"=' + id + '"}',
            data,
            config
          );
          if (res.data.status === 200) {
            this.addcabangerr = "";
            this.succes = true;
          }
        } catch (error) {
          console.log("error");
        }
      }
    },

    // aktivitas aset section

    emptyAktivitasAset() {
      this.exp_date = "";
      this.id_aktivitas = "";
      this.status_aktivitas = "";
      this.keterangan_aktivitas = "";
      this.foto_aktivitasaset = "";
    },

    async getSelectAktivitasAset(id) {
      console.log(id);
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };

      if (id) {
        try {
          const res = await axios.get(
            this.API + 'api/base/aset?where={"id":"=' + id + '"}',
            config
          );
          const data = res.data.result[0];
          const res2 = await axios.get(
            this.API + "api/extra/getaktivitasaset?id=" + data.id_grupaset,
            config
          );
          if (res2.status === 200) {
            this.aktivitasasetDropDown = res2.data.result;
            console.log(this.aktivitasasetDropDown);
          }
        } catch (error) {
          console.log(error);
        }
      } else {
        try {
          const res = await axios.get(
            this.API + 'api/base/aset?where={"id":"=' + this.id_aset + '"}',
            config
          );
          const data = res.data.result[0];
          const res2 = await axios.get(
            this.API + "api/extra/getaktivitasaset?id=" + data.id_grupaset,
            config
          );
          if (res2.status === 200) {
            this.aktivitasasetDropDown = res2.data.result;
          }
        } catch (error) {
          console.log(error);
        }
      }
    },

    async addAktivitasAset() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      let data = {
        id_aset: this.id_aset,
        id_aktivitas: this.id_aktivitas,
        expiry_date: dateFormat(this.exp_date),
        status_aktivasi: this.status_aktivitas === "Avilable" ? 1 : 2,
        keterangan: this.keterangan_aktivitas,
        image: this.foto_aktivitasaset,
      };

      try {
        const res = await axios.post(
          this.API + "api/base/aktivitas_aset",
          data,
          config
        );
        console.log(res);
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    async updateAktivitasAset(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      let data = {
        id_aset: this.id_aset,
        id_aktivitas: this.id_aktivitas,
        expiry_date: dateFormat(this.exp_date),
        status_aktivasi: this.status_aktivitas === "Avilable" ? 1 : 2,
        keterangan: this.keterangan_aktivitas,
        image: this.foto_aktivitasaset,
      };

      try {
        const res = await axios.put(
          this.API + 'api/base/aktivitas_aset?where={"id":"=' + id + '"}',
          data,
          config
        );
        console.log(res);
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    getAllAktivitasAset(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      axios
        .get(this.API + "api/extra/allaktivitasaset?id=" + id, config)
        .then((res) => {
          this.alldetailaset = res.data.result;
        });
    },

    async addLingkupKerja() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      let data = {
        id_aktivitas: this.id_aktivitas,
        lingkup_kerja: this.lingkupkerja,
        kondisi: 0,
        keterangan: this.keteranganlingkup,
      };

      try {
        const res = await axios.post(
          this.API + "api/base/lingkup_kerja",
          data,
          config
        );
        console.log(res);
        if (res.status === 200) {
          this.succes = true;
        }
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    async EditLingkupKerja(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      let data = {
        kondisi: this.kondisilingkup,
        keterangan: this.keteranganlingkup,
      };

      try {
        const res = await axios.put(
          this.API + 'api/base/lingkup_kerja?where={"id":"=' + id + '"}',
          data,
          config
        );
        console.log(res);
        if (res.status === 200) {
          this.succes = true;
        }
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    getLingkupKerja() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };

      this.lingkupkerja = "";
      try {
        axios
          .get(
            this.API +
              'api/base/lingkup_kerja?where={"id_aktivitas":"=' +
              this.id_aktivitas +
              '"}',
            config
          )
          .then((res) => {
            if (res.status === 200) {
              this.allLingkupKerja = res.data.result;
            }
          });
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    async deleteOneLingkup(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };

      this.lingkupkerja = "";
      try {
        const res = await axios.delete(
          this.API + 'api/base/lingkup_kerja?where={"id":"=' + id + '"}',
          config
        );
        if (res.status === 200) {
          this.succes = true;
        }
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    async getOneAktivitasAset(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };

      this.lingkupkerja = "";
      try {
        const res = await axios.get(
          this.API + "api/extra/getoneaktivitasaset?id=" + id,
          config
        );
        if (res.status === 200) {
          const data = res.data.result[0];
          this.id_aset = data.id_aset;
          this.nama_aset = data.nama_awal;
          (this.exp_date = dateFormat(data.expiry_date)),
            (this.id_aktivitas = data.id_aktivitas);
          this.status_aktivitas =
            data.status_aktivasi === 1 ? "Avilable" : "Expired";
          this.keterangan_aktivitas = data.keterangan;
          this.foto_aktivitasaset = data.image;
        }
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    // task section
    async getAsetRefCabang(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      this.asettaskselect = id;
      try {
        const res = await axios.get(
          this.API + "api/extra/getAsetRefCabang?id=" + id,
          config
        );

        this.allAsetCabang = res.data.result;
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },
    async getAktivitasAsetRefAset(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      this.aksettaskselect = id;
      try {
        const res = await axios.get(
          this.API + "api/extra/getAktivitasAsetRefAset?id=" + id,
          config
        );

        this.allAkivitasAset = res.data.result;
        console.log(this.allAkivitasAset);
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    async AddTask() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
          "Content-Type": "multipart/form-data",
        },
      };
      const data = {
        id_aktivitasaset: this.aksettaskselect,
        id_karyawan: cookie?.get("user").id,
        nama_task: this.nama_task,
        waktu_mulai: dateNumbers(this.waktu_mulai),
        waktu_sampai: dateNumbers(this.waktu_selesai),
        keterangan: this.keterangan_task,
        tanggal_operasional: new Date(),
        status: 1,
      };

      try {
        const res = await axios.post(this.API + "api/base/task", data, config);
        if (res.status === 200) {
          this.succes = true;
          this.asettaskselect = "";
          this.keterangan_task = "";
          this.waktu_mulai = "";
          this.waktu_selesai = "";
          this.nama_task = "";
        }
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    async RepeatTask() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
          "Content-Type": "multipart/form-data",
        },
      };
      const data = {
        id_aktivitasaset: this.id_aktivitasaset,
        id_karyawan: cookie?.get("user").id,
        nama_task: this.nama_awal,
        waktu_mulai: this.waktu_mulai,
        waktu_sampai: dateNumbers(this.tgl_expired),
        keterangan: this.keterangan_task,
        tanggal_operasional: new Date(),
        status: 1,
      };

      try {
        const res = await axios.post(this.API + "api/base/task", data, config);
        if (res.status === 200) {
          this.succes = true;
          this.asettaskselect = "";
          this.keterangan_task = "";
          this.waktu_mulai = "";
          this.tgl_expired = "";
          this.nama_task = "";
        }
      } catch (error) {
        this.succes = false;
        console.log(error);
      }
    },

    getAllTask() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      axios.get(this.API + "api/extra/getalltask", config).then((res) => {
        if (res.status === 200) {
          this.AllTask = res.data.result;
        }
      });
    },

    async getDetailTaskCabang(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      await axios
        .get(this.API + "api/extra/getDetailTask?id=" + id, config)
        .then((res) => {
          const data = res.data.result[0];
          console.log(data);
          this.id_aktivitasaset = data.id_aktivitasaset;
          this.aksettaskselect = data.id_aktivitasaset;
          this.id_aktivitasTask = data.id_aktivitas;
          this.cabangtaskselect = data.nama_cabang;
          this.asettaskselect = data.nama_awal;
          this.jabatanselect = data.jabatan;
          this.aksettaskselect = data.nama_aktivitas;
          this.waktu_mulai = dateFormat(data.waktu_mulai);
          this.keterangan_task = data.keterangan;
          this.tgl_expired = dateFormat(data.waktu_sampai);
          this.lokasi_aset = data.lokasi;
        });
      if (this.id_aktivitasTask) {
        axios
          .get(
            this.API +
              'api/base/lingkup_kerja?where={"id_aktivitas":"=' +
              this.id_aktivitasTask +
              '"}',
            config
          )
          .then((res) => {
            this.AllLingkuKerjaTask = res.data.result;
          });
      }
    },

    async checkTask() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      try {
        const res = await axios.get(
          this.API + `api/extra/checkstatustask?id=` + cookie.get("user").id,
          config
        );
        this.TaskNotClaim = res.data.result;
      } catch (err) {
        console.log(err);
      }
    },

    async getDetailTask(id) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      try {
        const res = await axios.get(
          this.API + "api/extra/getDetailTask?id=" + id,
          config
        );
        if (res.status === 200) {
          const data = res.data.result[0];
          this.id_aktivitasaset = data.id_aktivitasaset;
          this.aksettaskselect = data.id_aktivitasaset;
          this.id_aktivitasTask = data.id_aktivitas;
          this.cabangtaskselect = data.nama_cabang;
          this.asettaskselect = data.nama_awal;
          this.jabatanselect = data.jabatan;
          this.aksettaskselect = data.nama_aktivitas;
          this.waktu_mulai = dateFormat(data.waktu_mulai);
          this.keterangan_task = data.keterangan;
          this.tgl_expired = dateFormat(data.waktu_sampai);
          this.lokasi_aset = data.lokasi_aset;
        }
        if (this.id_aktivitasTask) {
          const res2 = await axios.get(
            this.API +
              'api/base/lingkup_kerja?where={"id_aktivitas":"=' +
              this.id_aktivitasTask +
              '"}',
            config
          );
          if (res2.status === 200) {
            this.AllLingkuKerjaTask = res2.data.result;
          }
        }
      } catch (error) {
        console.log(error);
      }
    },

    async ChangeStatusTask(idtask, iduser) {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      try {
        const res = await axios.put(
          this.API + '/api/base/task?where={"id":"=' + idtask + '"}',
          { status: iduser },
          config
        );
        console.log(res);
      } catch (error) {
        console.log(error);
      }
    },

    // detail task
    async AddDetailAktivitasAset() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
          "Content-Type": "application/json",
        },
      };
      const data = {
        id_AktivitasAset: this.id_aktivitasTask,
        foto_operasional: this.foto_operasional,
      };
      console.log(data);
      try {
        const res = await axios.post(
          this.API + "/api/extra/imageoperasional",
          data,
          config
        );
        if (res.status === 200) {
          this.foto_operasional = [];
        }
      } catch (error) {
        console.log(error);
      }
    },

    async GetFotoOperasional() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
        },
      };
      await axios
        .get(
          this.API +
            'api/base/detail_aktivitas_aset?where={"id_aktivitasaset":"=' +
            this.id_aktivitasTask +
            '"}',
          config
        )
        .then((res) => {
          this.FotoOperasionals = res.data.result;
        });
    },

    async getLogHistory() {
      const config = {
        headers: {
          Authorization: "Bearer " + this?.Token2,
          "Content-Type": "multipart/form-data",
        },
      };

      const data = {
        waktu_mulai: this.waktu_mulai,
        waktu_selesai: this.waktu_selesai,
      };

      try {
        await axios
          .post(this.API + "/api/extra/loghistory", data, config)
          .then((res) => {
            this.LogData = res.data.result;
            console.log(this.LogData);
            this.waktu_mulai = "";
            this.waktu_selesai = "";
          });
      } catch (error) {
        console.log(error);
      }
    },
  },
});
