import moment from 'moment';

export function dateNumbers(date) {
    date = new Date(date)
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    return [year, month, day].join("-");
}
   

export function dateFormat(date) {
    date = new Date(date)
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    return new Date([year, month, day].join("-"));
}


export function formattedDate(date) {
    return moment(date).format('YY-MM-DD');
  }