
import { createWebHistory, createRouter } from "vue-router";
import LoginPage from "@/components/login/LoginPage.vue";
import MenuPage from "@/components/MenuPage.vue";
import DepartmentPage from "@/components/DepartmentPage.vue";
import EmployeePage from "@/components/EmployeePage.vue";
import AddKaryawanPage from "@/components/karyawan/AddKaryawanPage";
import KaryawanFiturPage from "@/components/karyawan/KaryawanFiturPage.vue";
import DetailKaryawan from "@/components/karyawan/DetailKaryawan.vue";
import ViewKaryawan from "@/components/karyawan/ViewKaryawan.vue"
import EditKaryawan from "@/components/karyawan/EditKaryawan.vue"
import PerbantuanKaryawan from "@/components/karyawan/PerbantuanKaryawan.vue"
import FiturPage from "@/components/FiturPage.vue";
import ProdukPage from "@/components/ProdukPage";
import KategoriPage from "@/components/kategori/KategoriPage";
import AddKategoriPage from "@/components/kategori/AddKategoriPage";
import ViewAktivitasPage from "@/components/aktivitas/ViewAktivitasPage";
import GrupPage from "@/components/grup/GrupPage";
import AsetPage from "@/components/aset/AsetPage";
import AddAsetPage from "@/components/aset/AddAsetPage";
import EditAsetPage from "@/components/aset/EditAsetPage";
import DetailAsetPage from "@/components/aset/DetailAsetPage";
import AddAktivitasAset from "@/components/aktivitasaset/AddAktivitasAset";
import EditAktivitasAset from "@/components/aktivitasaset/EditAktivitasAset";
import CabangPage from "@/components/cabang/CabangPage";
import AddCabangPage from "@/components/cabang/AddCabangPage";
import ViewTaskPage from "@/components/task/ViewTaskPage";
import ViewTaskCabang from "@/components/task/ViewTaskCabang";
import DetailTask from "@/components/task/DetailTask";
import LogHistory from "@/components/task/LogHistory";
import LogHistoryAll from "@/components/task/LogHistoryAll";
import HelloWorld from "@/components/HelloWorld";
import MainLayout from '@/components/template/MainLayout';

const routes = [
  {path: "/",name: "LoginPage",component: LoginPage},
  {path: "/main",name: "MainLayout",component: MainLayout, children:[
    {path: "/menu",name: "MenuPage",component: MenuPage},
    {path: "/department",name: "",component: DepartmentPage},
    {path: "/employee",name: "EmployeePage",component: EmployeePage},

    //karyawan section    
    {path: "/addkaryawan",name: "AddKaryawanPage",component: AddKaryawanPage},
    {path: "/viewkaryawan",name: "ViewkaryawanPage",component: ViewKaryawan},
    {path: "/detailkaryawan/:id",name: "DetailKaryawan",component: DetailKaryawan},
    {path: "/editkaryawan/:id",name: "EditKaryawanPage",component: EditKaryawan},
    {path: "/perbantuankaryawan/:id",name: "PerbantuanKaryawan",component: PerbantuanKaryawan},
    {path: "/fitur",name: "FiturPage",component: FiturPage},
    {path: "/addfitur",name: "KaryawanFiturPage",component: KaryawanFiturPage},
    
    // {path: "/edit-fitur/:id",name: "EditKaryawanPage",component: KaryawanFiturPage},
    {path: "/produk",name: "ProdukPage",component: ProdukPage},

    // kategori page
    {path: "/kategori",name: "KategoriPage",component: KategoriPage},
    {path: "/addkategori",name: "AddKategoriPage",component: AddKategoriPage},
    
    //aktivitas routes
    {path: "/aktivitas",name: "ViewAktivitasPage",component: ViewAktivitasPage},

    //Grup page
    {path: "/grup",name: "GrupPage",component: GrupPage},

    //Aset
    {path: "/aset",name: "AsetPage",component: AsetPage},
    {path: "/addaset",name: "AddAsetPage",component: AddAsetPage},
    {path: "/editaset/:id",name: "EditAsetPage",component: EditAsetPage},
    {path: "/detailaset/:id",name: "DetailAsetPage",component: DetailAsetPage},
    
    // aktivitas aset
    {path: "/addaktivitasaset/:id",name: "AddAktivitasAset",component: AddAktivitasAset},
    {path: "/editaktivitasaset/:id",name: "EditAktivitasAset",component: EditAktivitasAset},
    
    //Cabang
    {path: "/cabang",name: "CabangPage",component: CabangPage},
    {path: "/addcabang",name: "AddCabangPage",component: AddCabangPage},

    // Task  
    {path: "/task",name: "ViewTaskPage",component: ViewTaskPage},
    {path: "/taskcabang",name: "ViewTaskCabang",component: ViewTaskCabang},
    {path: "/loghistorycabang",name: "LogHistory",component: LogHistory},
    {path: "/loghistory",name: "LogHistoryAll",component: LogHistoryAll},
    {path: "/detailtask/:id",name: "DetailTask",component: DetailTask},
    

    // caoba
    {path: "/heh",name: "heh",component: HelloWorld},
    
  ]},
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  linkActiveClass: "selected",
});

export default router;