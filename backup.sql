toc.dat                                                                                             0000600 0004000 0002000 00000070116 14502513525 0014446 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PGDMP           7                {            jaya    15.4    15.4 g    ~           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                    0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false         �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false         �           1262    16769    jaya    DATABASE     {   CREATE DATABASE jaya WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_Indonesia.1252';
    DROP DATABASE jaya;
                postgres    false         �            1259    16770    cabang    TABLE     �   CREATE TABLE public.cabang (
    id integer NOT NULL,
    nama character varying(50),
    alamat character varying(200),
    hp character varying(15),
    foto character varying(50),
    pj integer,
    status integer,
    persen_produk integer
);
    DROP TABLE public.cabang;
       public         heap    postgres    false         �            1259    16773    cabang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cabang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.cabang_id_seq;
       public          postgres    false    214         �           0    0    cabang_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.cabang_id_seq OWNED BY public.cabang.id;
          public          postgres    false    215         �            1259    16774    detailkeranjang    TABLE     �   CREATE TABLE public.detailkeranjang (
    id integer NOT NULL,
    id_order character varying(50),
    id_cabang integer,
    id_produk integer,
    harga_cabang integer,
    harga_asli integer,
    status integer
);
 #   DROP TABLE public.detailkeranjang;
       public         heap    postgres    false         �            1259    16777    detailkeranjang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.detailkeranjang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.detailkeranjang_id_seq;
       public          postgres    false    216         �           0    0    detailkeranjang_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.detailkeranjang_id_seq OWNED BY public.detailkeranjang.id;
          public          postgres    false    217         �            1259    16778    fitur    TABLE     w   CREATE TABLE public.fitur (
    id integer NOT NULL,
    fitur character varying(50),
    url character varying(50)
);
    DROP TABLE public.fitur;
       public         heap    postgres    false         �            1259    16781    fitur_id_seq    SEQUENCE     �   CREATE SEQUENCE public.fitur_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.fitur_id_seq;
       public          postgres    false    218         �           0    0    fitur_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.fitur_id_seq OWNED BY public.fitur.id;
          public          postgres    false    219         �            1259    16782 
   fotoproduk    TABLE     s   CREATE TABLE public.fotoproduk (
    id integer NOT NULL,
    id_produk integer,
    foto character varying(50)
);
    DROP TABLE public.fotoproduk;
       public         heap    postgres    false         �            1259    16785    fotoproduk_id_seq    SEQUENCE     �   CREATE SEQUENCE public.fotoproduk_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.fotoproduk_id_seq;
       public          postgres    false    220         �           0    0    fotoproduk_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.fotoproduk_id_seq OWNED BY public.fotoproduk.id;
          public          postgres    false    221         �            1259    16786    jabatan    TABLE     \   CREATE TABLE public.jabatan (
    id integer NOT NULL,
    jabatan character varying(50)
);
    DROP TABLE public.jabatan;
       public         heap    postgres    false         �            1259    16789    jabatan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.jabatan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.jabatan_id_seq;
       public          postgres    false    222         �           0    0    jabatan_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.jabatan_id_seq OWNED BY public.jabatan.id;
          public          postgres    false    223         �            1259    16790    jenis    TABLE     X   CREATE TABLE public.jenis (
    id integer NOT NULL,
    jenis character varying(50)
);
    DROP TABLE public.jenis;
       public         heap    postgres    false         �            1259    16793    jenis_id_seq    SEQUENCE     �   CREATE SEQUENCE public.jenis_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.jenis_id_seq;
       public          postgres    false    224         �           0    0    jenis_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.jenis_id_seq OWNED BY public.jenis.id;
          public          postgres    false    225         �            1259    16794    kategori    TABLE     ^   CREATE TABLE public.kategori (
    id integer NOT NULL,
    kategori character varying(50)
);
    DROP TABLE public.kategori;
       public         heap    postgres    false         �            1259    16797    kategori_id_seq    SEQUENCE     �   CREATE SEQUENCE public.kategori_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.kategori_id_seq;
       public          postgres    false    226         �           0    0    kategori_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.kategori_id_seq OWNED BY public.kategori.id;
          public          postgres    false    227         �            1259    16798 	   keranjang    TABLE     �  CREATE TABLE public.keranjang (
    id integer NOT NULL,
    nama character varying(50),
    alamat character varying(250),
    id_order character varying(50),
    id_cabang integer,
    hp character varying(15),
    total integer,
    status character varying(2),
    tgl_transaksi date,
    tgl_bayar date,
    potongan integer,
    id_user integer,
    tipe_bayar character varying(50),
    keterangan character varying(250),
    id_voucher integer
);
    DROP TABLE public.keranjang;
       public         heap    postgres    false         �            1259    16803    keranjang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.keranjang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.keranjang_id_seq;
       public          postgres    false    228         �           0    0    keranjang_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.keranjang_id_seq OWNED BY public.keranjang.id;
          public          postgres    false    229         �            1259    16804    login    TABLE     �  CREATE TABLE public.login (
    id integer NOT NULL,
    nama character varying(50),
    username character varying(50) NOT NULL,
    password character varying(32) NOT NULL,
    hp character varying(15),
    email character varying(50),
    foto character varying(50),
    token character varying(50),
    id_jabatan integer,
    status integer,
    tgl_buat date,
    tgl_masuk date,
    alamat character varying(200),
    nik character varying(16),
    id_cabang integer
);
    DROP TABLE public.login;
       public         heap    postgres    false         �            1259    16809    login_id_seq    SEQUENCE     �   CREATE SEQUENCE public.login_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.login_id_seq;
       public          postgres    false    230         �           0    0    login_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.login_id_seq OWNED BY public.login.id;
          public          postgres    false    231         �            1259    16810    produk    TABLE     U  CREATE TABLE public.produk (
    id integer NOT NULL,
    nama character varying(50),
    deskripsi character varying(250),
    id_jenis integer,
    id_kategori integer,
    warna character varying(30),
    keterangan character varying(250),
    status integer,
    tgl_input date,
    tgl_barcode date,
    tgl_laku date,
    barcode character varying(50),
    foto character varying(50),
    merek character varying(50),
    material character varying(50),
    dimensi character varying(30),
    id_cabang integer,
    tgl_display date,
    harga integer,
    no_surat character varying(50)
);
    DROP TABLE public.produk;
       public         heap    postgres    false         �            1259    16815    produk_id_seq    SEQUENCE     �   CREATE SEQUENCE public.produk_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.produk_id_seq;
       public          postgres    false    232         �           0    0    produk_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.produk_id_seq OWNED BY public.produk.id;
          public          postgres    false    233         �            1259    16816    status    TABLE     Z   CREATE TABLE public.status (
    id integer NOT NULL,
    status character varying(15)
);
    DROP TABLE public.status;
       public         heap    postgres    false         �            1259    16819    status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.status_id_seq;
       public          postgres    false    234         �           0    0    status_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.status_id_seq OWNED BY public.status.id;
          public          postgres    false    235         �            1259    16820    transfer    TABLE     6  CREATE TABLE public.transfer (
    id integer NOT NULL,
    id_produk integer,
    cabang_asal integer,
    cabang_tujuan integer,
    id_status integer,
    no_surat character varying(50),
    nama_kirim character varying(50),
    nama_terima character varying(50),
    tgl_kirim date,
    tgl_terima date
);
    DROP TABLE public.transfer;
       public         heap    postgres    false         �            1259    16823    transfer_id_seq    SEQUENCE     �   CREATE SEQUENCE public.transfer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.transfer_id_seq;
       public          postgres    false    236         �           0    0    transfer_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.transfer_id_seq OWNED BY public.transfer.id;
          public          postgres    false    237         �            1259    16824 	   userfitur    TABLE     f   CREATE TABLE public.userfitur (
    id integer NOT NULL,
    id_user integer,
    id_fitur integer
);
    DROP TABLE public.userfitur;
       public         heap    postgres    false         �            1259    16827    userfitur_id_seq    SEQUENCE     �   CREATE SEQUENCE public.userfitur_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.userfitur_id_seq;
       public          postgres    false    238         �           0    0    userfitur_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.userfitur_id_seq OWNED BY public.userfitur.id;
          public          postgres    false    239         �            1259    16828    voucher    TABLE     \  CREATE TABLE public.voucher (
    id integer NOT NULL,
    kode character varying(50),
    nama character varying(50),
    deskripsi character varying(250),
    nominal integer,
    jumlah integer,
    terpakai integer,
    sisa integer,
    tipe character varying(10),
    tgl_berlaku date,
    tgl_expire date,
    status character varying(2)
);
    DROP TABLE public.voucher;
       public         heap    postgres    false         �            1259    16831    voucher_id_seq    SEQUENCE     �   CREATE SEQUENCE public.voucher_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.voucher_id_seq;
       public          postgres    false    240         �           0    0    voucher_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.voucher_id_seq OWNED BY public.voucher.id;
          public          postgres    false    241         �           2604    16832 	   cabang id    DEFAULT     f   ALTER TABLE ONLY public.cabang ALTER COLUMN id SET DEFAULT nextval('public.cabang_id_seq'::regclass);
 8   ALTER TABLE public.cabang ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    215    214         �           2604    16833    detailkeranjang id    DEFAULT     x   ALTER TABLE ONLY public.detailkeranjang ALTER COLUMN id SET DEFAULT nextval('public.detailkeranjang_id_seq'::regclass);
 A   ALTER TABLE public.detailkeranjang ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    217    216         �           2604    16834    fitur id    DEFAULT     d   ALTER TABLE ONLY public.fitur ALTER COLUMN id SET DEFAULT nextval('public.fitur_id_seq'::regclass);
 7   ALTER TABLE public.fitur ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    219    218         �           2604    16835    fotoproduk id    DEFAULT     n   ALTER TABLE ONLY public.fotoproduk ALTER COLUMN id SET DEFAULT nextval('public.fotoproduk_id_seq'::regclass);
 <   ALTER TABLE public.fotoproduk ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    221    220         �           2604    16836 
   jabatan id    DEFAULT     h   ALTER TABLE ONLY public.jabatan ALTER COLUMN id SET DEFAULT nextval('public.jabatan_id_seq'::regclass);
 9   ALTER TABLE public.jabatan ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    223    222         �           2604    16837    jenis id    DEFAULT     d   ALTER TABLE ONLY public.jenis ALTER COLUMN id SET DEFAULT nextval('public.jenis_id_seq'::regclass);
 7   ALTER TABLE public.jenis ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    225    224         �           2604    16838    kategori id    DEFAULT     j   ALTER TABLE ONLY public.kategori ALTER COLUMN id SET DEFAULT nextval('public.kategori_id_seq'::regclass);
 :   ALTER TABLE public.kategori ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    227    226         �           2604    16839    keranjang id    DEFAULT     l   ALTER TABLE ONLY public.keranjang ALTER COLUMN id SET DEFAULT nextval('public.keranjang_id_seq'::regclass);
 ;   ALTER TABLE public.keranjang ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    229    228         �           2604    16840    login id    DEFAULT     d   ALTER TABLE ONLY public.login ALTER COLUMN id SET DEFAULT nextval('public.login_id_seq'::regclass);
 7   ALTER TABLE public.login ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    231    230         �           2604    16841 	   produk id    DEFAULT     f   ALTER TABLE ONLY public.produk ALTER COLUMN id SET DEFAULT nextval('public.produk_id_seq'::regclass);
 8   ALTER TABLE public.produk ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    233    232         �           2604    16842 	   status id    DEFAULT     f   ALTER TABLE ONLY public.status ALTER COLUMN id SET DEFAULT nextval('public.status_id_seq'::regclass);
 8   ALTER TABLE public.status ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    235    234         �           2604    16843    transfer id    DEFAULT     j   ALTER TABLE ONLY public.transfer ALTER COLUMN id SET DEFAULT nextval('public.transfer_id_seq'::regclass);
 :   ALTER TABLE public.transfer ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    237    236         �           2604    16844    userfitur id    DEFAULT     l   ALTER TABLE ONLY public.userfitur ALTER COLUMN id SET DEFAULT nextval('public.userfitur_id_seq'::regclass);
 ;   ALTER TABLE public.userfitur ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    239    238         �           2604    16845 
   voucher id    DEFAULT     h   ALTER TABLE ONLY public.voucher ALTER COLUMN id SET DEFAULT nextval('public.voucher_id_seq'::regclass);
 9   ALTER TABLE public.voucher ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    241    240         `          0    16770    cabang 
   TABLE DATA           W   COPY public.cabang (id, nama, alamat, hp, foto, pj, status, persen_produk) FROM stdin;
    public          postgres    false    214       3424.dat b          0    16774    detailkeranjang 
   TABLE DATA           o   COPY public.detailkeranjang (id, id_order, id_cabang, id_produk, harga_cabang, harga_asli, status) FROM stdin;
    public          postgres    false    216       3426.dat d          0    16778    fitur 
   TABLE DATA           /   COPY public.fitur (id, fitur, url) FROM stdin;
    public          postgres    false    218       3428.dat f          0    16782 
   fotoproduk 
   TABLE DATA           9   COPY public.fotoproduk (id, id_produk, foto) FROM stdin;
    public          postgres    false    220       3430.dat h          0    16786    jabatan 
   TABLE DATA           .   COPY public.jabatan (id, jabatan) FROM stdin;
    public          postgres    false    222       3432.dat j          0    16790    jenis 
   TABLE DATA           *   COPY public.jenis (id, jenis) FROM stdin;
    public          postgres    false    224       3434.dat l          0    16794    kategori 
   TABLE DATA           0   COPY public.kategori (id, kategori) FROM stdin;
    public          postgres    false    226       3436.dat n          0    16798 	   keranjang 
   TABLE DATA           �   COPY public.keranjang (id, nama, alamat, id_order, id_cabang, hp, total, status, tgl_transaksi, tgl_bayar, potongan, id_user, tipe_bayar, keterangan, id_voucher) FROM stdin;
    public          postgres    false    228       3438.dat p          0    16804    login 
   TABLE DATA           �   COPY public.login (id, nama, username, password, hp, email, foto, token, id_jabatan, status, tgl_buat, tgl_masuk, alamat, nik, id_cabang) FROM stdin;
    public          postgres    false    230       3440.dat r          0    16810    produk 
   TABLE DATA           �   COPY public.produk (id, nama, deskripsi, id_jenis, id_kategori, warna, keterangan, status, tgl_input, tgl_barcode, tgl_laku, barcode, foto, merek, material, dimensi, id_cabang, tgl_display, harga, no_surat) FROM stdin;
    public          postgres    false    232       3442.dat t          0    16816    status 
   TABLE DATA           ,   COPY public.status (id, status) FROM stdin;
    public          postgres    false    234       3444.dat v          0    16820    transfer 
   TABLE DATA           �   COPY public.transfer (id, id_produk, cabang_asal, cabang_tujuan, id_status, no_surat, nama_kirim, nama_terima, tgl_kirim, tgl_terima) FROM stdin;
    public          postgres    false    236       3446.dat x          0    16824 	   userfitur 
   TABLE DATA           :   COPY public.userfitur (id, id_user, id_fitur) FROM stdin;
    public          postgres    false    238       3448.dat z          0    16828    voucher 
   TABLE DATA           �   COPY public.voucher (id, kode, nama, deskripsi, nominal, jumlah, terpakai, sisa, tipe, tgl_berlaku, tgl_expire, status) FROM stdin;
    public          postgres    false    240       3450.dat �           0    0    cabang_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.cabang_id_seq', 2, true);
          public          postgres    false    215         �           0    0    detailkeranjang_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.detailkeranjang_id_seq', 1, false);
          public          postgres    false    217         �           0    0    fitur_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.fitur_id_seq', 9, true);
          public          postgres    false    219         �           0    0    fotoproduk_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.fotoproduk_id_seq', 1, false);
          public          postgres    false    221         �           0    0    jabatan_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.jabatan_id_seq', 11, true);
          public          postgres    false    223         �           0    0    jenis_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.jenis_id_seq', 4, true);
          public          postgres    false    225         �           0    0    kategori_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.kategori_id_seq', 4, true);
          public          postgres    false    227         �           0    0    keranjang_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.keranjang_id_seq', 1, false);
          public          postgres    false    229         �           0    0    login_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.login_id_seq', 20, true);
          public          postgres    false    231         �           0    0    produk_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.produk_id_seq', 6, true);
          public          postgres    false    233         �           0    0    status_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.status_id_seq', 7, true);
          public          postgres    false    235         �           0    0    transfer_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.transfer_id_seq', 31, true);
          public          postgres    false    237         �           0    0    userfitur_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.userfitur_id_seq', 24, true);
          public          postgres    false    239         �           0    0    voucher_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.voucher_id_seq', 1, false);
          public          postgres    false    241         �           2606    16847    cabang cabang_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.cabang
    ADD CONSTRAINT cabang_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.cabang DROP CONSTRAINT cabang_pkey;
       public            postgres    false    214         �           2606    16849 $   detailkeranjang detailkeranjang_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.detailkeranjang
    ADD CONSTRAINT detailkeranjang_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.detailkeranjang DROP CONSTRAINT detailkeranjang_pkey;
       public            postgres    false    216         �           2606    16851    fitur fitur_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.fitur
    ADD CONSTRAINT fitur_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.fitur DROP CONSTRAINT fitur_pkey;
       public            postgres    false    218         �           2606    16853    fotoproduk fotoproduk_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.fotoproduk
    ADD CONSTRAINT fotoproduk_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.fotoproduk DROP CONSTRAINT fotoproduk_pkey;
       public            postgres    false    220         �           2606    16855    jabatan jabatan_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.jabatan
    ADD CONSTRAINT jabatan_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.jabatan DROP CONSTRAINT jabatan_pkey;
       public            postgres    false    222         �           2606    16857    jenis jenis_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.jenis
    ADD CONSTRAINT jenis_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.jenis DROP CONSTRAINT jenis_pkey;
       public            postgres    false    224         �           2606    16859    kategori kategori_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.kategori DROP CONSTRAINT kategori_pkey;
       public            postgres    false    226         �           2606    16861    keranjang keranjang_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.keranjang
    ADD CONSTRAINT keranjang_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.keranjang DROP CONSTRAINT keranjang_pkey;
       public            postgres    false    228         �           2606    16863    login login_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.login
    ADD CONSTRAINT login_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.login DROP CONSTRAINT login_pkey;
       public            postgres    false    230         �           2606    16865    login login_username_key 
   CONSTRAINT     W   ALTER TABLE ONLY public.login
    ADD CONSTRAINT login_username_key UNIQUE (username);
 B   ALTER TABLE ONLY public.login DROP CONSTRAINT login_username_key;
       public            postgres    false    230         �           2606    16867    produk produk_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.produk
    ADD CONSTRAINT produk_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.produk DROP CONSTRAINT produk_pkey;
       public            postgres    false    232         �           2606    16869    status status_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.status DROP CONSTRAINT status_pkey;
       public            postgres    false    234         �           2606    16871    transfer transfer_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.transfer
    ADD CONSTRAINT transfer_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.transfer DROP CONSTRAINT transfer_pkey;
       public            postgres    false    236         �           2606    16873    userfitur userfitur_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.userfitur
    ADD CONSTRAINT userfitur_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.userfitur DROP CONSTRAINT userfitur_pkey;
       public            postgres    false    238         �           2606    16875    voucher voucher_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.voucher
    ADD CONSTRAINT voucher_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.voucher DROP CONSTRAINT voucher_pkey;
       public            postgres    false    240                                                                                                                                                                                                                                                                                                                                                                                                                                                          3424.dat                                                                                            0000600 0004000 0002000 00000000157 14502513525 0014253 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	pusat	jalan pusat no 1	325432	pusat.jpg	1	1	10
2	cabang satu	jalan cabang no 1	111111	cabang.jpg	1	1	15
\.


                                                                                                                                                                                                                                                                                                                                                                                                                 3426.dat                                                                                            0000600 0004000 0002000 00000000005 14502513525 0014245 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3428.dat                                                                                            0000600 0004000 0002000 00000000252 14502513525 0014253 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Department	/department
2	Employee	/employee
3	Karyawan	/karyawan
4	Fitur	/fitur
6	Produk	/produk
7	Transfer	/transfer
8	Transfer Terima	/terima
9	Barcode	/barcode
\.


                                                                                                                                                                                                                                                                                                                                                      3430.dat                                                                                            0000600 0004000 0002000 00000000005 14502513525 0014240 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3432.dat                                                                                            0000600 0004000 0002000 00000000063 14502513525 0014246 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	admin-it
6	owner
7	sales
8	kasir
11	manager
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                             3434.dat                                                                                            0000600 0004000 0002000 00000000046 14502513525 0014251 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Meja
2	Kursi
3	Tangga
4	Lemari
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          3436.dat                                                                                            0000600 0004000 0002000 00000000054 14502513525 0014252 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Kamar
2	Dapur
3	Ruang Tamu
4	Outdoor
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    3438.dat                                                                                            0000600 0004000 0002000 00000000005 14502513525 0014250 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3440.dat                                                                                            0000600 0004000 0002000 00000006034 14502513525 0014251 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	test	test	098f6bcd4621d373cade4e832627b4f6	081915532490	zen@intelove.com	foto.jpg	token1	1	1	2023-04-03	2023-04-03	jalan sana sini nomer 444	j-00123	1
2	false	false	false	false	false	false	false	1	1	2023-04-27	2023-12-12	false	false	1
3	zen	zen	7e9aedd97b5ec4590edb8281ff12b168	1332434	zen@inte.love	RF066PGZHstrbSK2	It0AHyfDGdZUMitjN7YQBkh8XPZZhi9X	1	1	2023-04-27	2023-12-12	ddfgdfgdfgdfg	34534634636	1
4	zen1	zen1	39465c1f88633bedfa396a8f4d3408eb	1332434	zen1@inte.love	RF066PGZHstrbSK2	gexALy6WEl88wLuITtDCh5Ou37kmBVlg	1	1	2023-04-27	2023-12-12	ddfgdfgdfgdfg	34534634636	1
5	zen12	zen12	0a9892b54daa41b3502d2ebc815e9228	1332434	zen12@inte.love	RF066PGZHstrbSK2	Ra9ShkkEu1Lou9DS5Ye70t6KsjG3B8Fg	1	1	2023-04-27	2023-12-12	ddfgdfgdfgdfg	34534634636	1
6	uye	uye	41b3b100f2c1686edef944ef821679b8	345345	ue@rdftg.dfg	zfC7oVR8hZGJcz9B	OfSau1W8LpyxXB14uMu3kTpDnHpaIhPR	1	1	2023-04-27	2024-11-11	fgh fgh f xhf fghx 	546547	1
8	uye	uye5	b337488e902d8b817e81dc7fc4f2c6fa	345345	ue@rdftg.dfg	zfC7oVR8hZGJcz9B	xhcGz2QyQ09XFMfUVz25OjiM7sGiPBka	1	1	2023-04-27	2024-11-11	fgh fgh f xhf fghx 	546547	1
9	ba	ba	07159c47ee1b19ae4fb9c40d480856c4	23	ba@bba.ge	anonymous.png	fuPbYVESxxeHYQfJeiSpIoJF0UOllUer	1	1	2023-04-27	2023-11-11	fdgdfg dfgdfzg dzfgzdg	356547	1
10	esdf	sd	6226f7cbe59e99a90b5cef6f94f966fd	3254	fo@sini.com	anonymous.png	Fm0FZ2MOCvb4aagNqZtgpdkTTVKRgeVR	2	1	2023-04-27	2222-12-22	fgdfgdfg	3455	1
11	gas	gas	fee74eac0728a1bb5ff7d4666f8c4a88	35235	gas@g.f	qtGkIVsHzWTwlMbb	fmuqyecoUVGRyaBLqKDtBccn9jlgC52B	11	1	2023-04-28	2023-04-02	fdgdf sgdss sdgsg	546546546	1
12	gas	gas1	b5a3413c6e8a1dda5849a1d678faad03	35235	gas@g.f	qtGkIVsHzWTwlMbb	adHTZJt8Z9WVB2RuHprXvcIXY1Kuia0m	11	1	2023-04-28	2023-04-02	fdgdf sgdss sdgsg	546546546	1
13	gas	gas2	23b7b0565681a25b85067c75ae618a4d	35235	gas@g.f	qtGkIVsHzWTwlMbb	J96JRe1772mxdyi4dqXQUN8UAnFprUYR	6	1	2023-04-28	2023-04-03	fdgdf sgdss sdgsg	546546546	1
14	tat	tat	872e384a07f7ea1e05fc742f5be6008b	34534534	ta@t.rf	KTDGouOeRiAj4Z4q	21CSclbqJs6jrHvbiXGtLTuKwv1zF4c7	8	1	2023-04-28	2023-04-17	fyhf rfh rtfhr trt rth	346346345	1
15	Moar	moar	2f5033d31cd4ed79bf72ed7225dcaa9f	234234	mo@r.o	51on4jYbixUhyD37	5IGSCtlTkETb2Pg1cuApEdsEX0Qpoiv4	8	1	2023-05-02	2023-04-03	dsfgd  dgdf dfg dmo 32r 	456546456	2
16	yes	yes	a6105c0a611b41b08f1209506350279e	32424	yes@ry.fdg	TiXbv5JYEBZVLau5	QPxFwk1QHiTUWD0BnYzF4LejkDP7qI6J	11	1	2023-05-02	2023-05-01	dg dfg dgdg  ege	4334654656	2
17	uyeyeye	uyeue	18672256eba154c307e9d4abed63560d	324234	ey@sgg.fhg	KSyOa4XY4MrcueLf	fGYbVuWjoPtymRG2REzzs0InaOZawqee	6	1	2023-05-02	2023-05-01	gfhf fgh fgh ghf fg h	5434656	2
18	saw	saw	a07a8f8eab60eaafe24af1016a02fbf3	32423535	saw@ed.fdg	xCN3DWcb7X1njYV0	YTObkMhePtLUoS8vAYBs6f6oIWGCnSEi	8	1	2023-05-10	2023-05-05	fghf fghf ghfthrt h rh	546547547457	2
19	daz	daz	7f9c5e64ca9ab8d2ee03704097fd58cc	45345345	daz@e.rt	DAVraakYutxB2Ghw	wJt76Um6KY6kMIndXzAwlmTHh1DBxTC4	7	1	2023-05-10	2023-05-01	dgdgf dg dhhd hdh	36363563	1
20	saya	saya	20c1a26a55039b30866c9d0aa51953ca	345345	sa@y.a	evvvd8JJo40GRUOe	XSdTKDOybRbhlyafwLegLHaJUyNwpalQ	11	1	2023-05-10	2023-04-12	fdgdfg dg de egr	544574574	1
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    3442.dat                                                                                            0000600 0004000 0002000 00000001543 14502513525 0014253 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        4	Lemari Display	Lemari display yang cocok untuk dipamerkan di ruang tamu.	4	3	Putih	Lemari Display Kaca Bening	1	2023-05-15	\N	\N	1685932425	mJYoXNYP1BLeFoIH	Uyeye	Kaca	100cm x 50cm x 150cm	1	\N	\N	ST-16925833881
1	Meja Dapur	Meja dapur dengan lemari 3 pintu samping dan 2 pintu bawah	1	2	Biru	Custom Premium	1	2023-03-12	\N	\N	1685932393	51on4jYbixUhyD37	Nguyen	Aluminium	1,5m x 2m x 0,5m	1	\N	\N	ST-16925834891
2	Tangga 2 Meter	Tangga lipat yang bisa mencapai panjang  maksimal 2 meter	1	4	Hitam	Tangga Lipat	1	2023-04-04	\N	\N	1685932434	8lVC91zOJ3Qmn4HI	Sanyo	Aluminium	2m x 0,25m x 0,1m	1	\N	\N	ST-16925835411
3	Kursi Gaming	Kursi gaming murah terbuat dari plastik dengan desain kekinian. Cocok untuk gamer pemula.	2	1	Ungu	Kursi gaming murah kekinian	1	2023-05-15	\N	\N	1685932416	G8qVGu9rXHHBjfYX	Meruk	Plastik	30cm x 30cm x 100cm	1	\N	\N	ST-16925835411
\.


                                                                                                                                                             3444.dat                                                                                            0000600 0004000 0002000 00000000077 14502513525 0014256 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	True
2	False
3	Pending
4	Cancel
5	Done
6	Barcode
7	Laku
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                 3446.dat                                                                                            0000600 0004000 0002000 00000002521 14502513525 0014254 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	3	1	\N	2	ST-16859359591	test	\N	2023-06-05	\N
2	2	1	\N	2	ST-16859359591	test	\N	2023-06-05	\N
3	4	1	\N	2	ST-16859367501	test	\N	2023-06-05	\N
4	3	1	\N	2	ST-16859367501	test	\N	2023-06-05	\N
5	1	1	\N	2	ST-16859367501	test	\N	2023-06-05	\N
6	2	1	\N	2	ST-16859367501	test	\N	2023-06-05	\N
7	2	1	\N	2	ST-16859367501	test	\N	2023-06-05	\N
8	2	1	\N	2	ST-16859367501	test	\N	2023-06-05	\N
9	4	1	\N	2	ST-16859375001	test	\N	2023-06-05	\N
12	1	1	\N	2	ST-16859473541	test	\N	2023-06-05	\N
13	4	1	\N	2	ST-16859473541	test	\N	2023-06-05	\N
14	2	1	\N	2	ST-16859474671	test	\N	2023-06-05	\N
15	3	1	\N	2	ST-16859474671	test	\N	2023-06-05	\N
16	3	1	\N	2	ST-16859476361	test	\N	2023-06-05	\N
17	1	1	\N	2	ST-16859476361	test	\N	2023-06-05	\N
18	3	1	2	1	ST-16859478021	test	\N	2023-06-05	\N
19	1	1	2	1	ST-16859478021	test	\N	2023-06-05	\N
20	3	1	2	1	ST-16859479451	test	\N	2023-06-05	\N
21	4	1	2	1	ST-16859479451	test	\N	2023-06-05	\N
22	2	1	2	1	ST-16859479451	test	\N	2023-06-05	\N
23	3	1	\N	2	ST-16921513571	test	\N	2023-08-16	\N
25	2	1	\N	2	ST-16921513571	test	\N	2023-08-16	\N
26	4	1	\N	2	ST-16925833881	test	\N	2023-08-21	\N
27	1	1	\N	2	ST-16925833881	test	\N	2023-08-21	\N
28	3	1	2	1	ST-16925834891	test	\N	2023-08-21	\N
29	1	1	2	1	ST-16925834891	test	\N	2023-08-21	\N
30	2	1	\N	2	ST-16925835411	test	\N	2023-08-21	\N
31	3	1	\N	2	ST-16925835411	test	\N	2023-08-21	\N
\.


                                                                                                                                                                               3448.dat                                                                                            0000600 0004000 0002000 00000000116 14502513525 0014254 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	1	1
2	1	2
3	1	3
4	1	4
5	9	3
6	13	1
18	9	2
19	1	6
20	1	7
21	16	8
22	9	8
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                  3450.dat                                                                                            0000600 0004000 0002000 00000000005 14502513525 0014242 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           restore.sql                                                                                         0000600 0004000 0002000 00000056336 14502513525 0015403 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        --
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 15.4
-- Dumped by pg_dump version 15.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE jaya;
--
-- Name: jaya; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE jaya WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_Indonesia.1252';


ALTER DATABASE jaya OWNER TO postgres;

\connect jaya

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: cabang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cabang (
    id integer NOT NULL,
    nama character varying(50),
    alamat character varying(200),
    hp character varying(15),
    foto character varying(50),
    pj integer,
    status integer,
    persen_produk integer
);


ALTER TABLE public.cabang OWNER TO postgres;

--
-- Name: cabang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cabang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cabang_id_seq OWNER TO postgres;

--
-- Name: cabang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cabang_id_seq OWNED BY public.cabang.id;


--
-- Name: detailkeranjang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detailkeranjang (
    id integer NOT NULL,
    id_order character varying(50),
    id_cabang integer,
    id_produk integer,
    harga_cabang integer,
    harga_asli integer,
    status integer
);


ALTER TABLE public.detailkeranjang OWNER TO postgres;

--
-- Name: detailkeranjang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detailkeranjang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detailkeranjang_id_seq OWNER TO postgres;

--
-- Name: detailkeranjang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detailkeranjang_id_seq OWNED BY public.detailkeranjang.id;


--
-- Name: fitur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fitur (
    id integer NOT NULL,
    fitur character varying(50),
    url character varying(50)
);


ALTER TABLE public.fitur OWNER TO postgres;

--
-- Name: fitur_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fitur_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fitur_id_seq OWNER TO postgres;

--
-- Name: fitur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fitur_id_seq OWNED BY public.fitur.id;


--
-- Name: fotoproduk; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fotoproduk (
    id integer NOT NULL,
    id_produk integer,
    foto character varying(50)
);


ALTER TABLE public.fotoproduk OWNER TO postgres;

--
-- Name: fotoproduk_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fotoproduk_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fotoproduk_id_seq OWNER TO postgres;

--
-- Name: fotoproduk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fotoproduk_id_seq OWNED BY public.fotoproduk.id;


--
-- Name: jabatan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jabatan (
    id integer NOT NULL,
    jabatan character varying(50)
);


ALTER TABLE public.jabatan OWNER TO postgres;

--
-- Name: jabatan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jabatan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jabatan_id_seq OWNER TO postgres;

--
-- Name: jabatan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jabatan_id_seq OWNED BY public.jabatan.id;


--
-- Name: jenis; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jenis (
    id integer NOT NULL,
    jenis character varying(50)
);


ALTER TABLE public.jenis OWNER TO postgres;

--
-- Name: jenis_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jenis_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jenis_id_seq OWNER TO postgres;

--
-- Name: jenis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jenis_id_seq OWNED BY public.jenis.id;


--
-- Name: kategori; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kategori (
    id integer NOT NULL,
    kategori character varying(50)
);


ALTER TABLE public.kategori OWNER TO postgres;

--
-- Name: kategori_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.kategori_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kategori_id_seq OWNER TO postgres;

--
-- Name: kategori_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.kategori_id_seq OWNED BY public.kategori.id;


--
-- Name: keranjang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.keranjang (
    id integer NOT NULL,
    nama character varying(50),
    alamat character varying(250),
    id_order character varying(50),
    id_cabang integer,
    hp character varying(15),
    total integer,
    status character varying(2),
    tgl_transaksi date,
    tgl_bayar date,
    potongan integer,
    id_user integer,
    tipe_bayar character varying(50),
    keterangan character varying(250),
    id_voucher integer
);


ALTER TABLE public.keranjang OWNER TO postgres;

--
-- Name: keranjang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.keranjang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.keranjang_id_seq OWNER TO postgres;

--
-- Name: keranjang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.keranjang_id_seq OWNED BY public.keranjang.id;


--
-- Name: login; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.login (
    id integer NOT NULL,
    nama character varying(50),
    username character varying(50) NOT NULL,
    password character varying(32) NOT NULL,
    hp character varying(15),
    email character varying(50),
    foto character varying(50),
    token character varying(50),
    id_jabatan integer,
    status integer,
    tgl_buat date,
    tgl_masuk date,
    alamat character varying(200),
    nik character varying(16),
    id_cabang integer
);


ALTER TABLE public.login OWNER TO postgres;

--
-- Name: login_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.login_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.login_id_seq OWNER TO postgres;

--
-- Name: login_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.login_id_seq OWNED BY public.login.id;


--
-- Name: produk; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.produk (
    id integer NOT NULL,
    nama character varying(50),
    deskripsi character varying(250),
    id_jenis integer,
    id_kategori integer,
    warna character varying(30),
    keterangan character varying(250),
    status integer,
    tgl_input date,
    tgl_barcode date,
    tgl_laku date,
    barcode character varying(50),
    foto character varying(50),
    merek character varying(50),
    material character varying(50),
    dimensi character varying(30),
    id_cabang integer,
    tgl_display date,
    harga integer,
    no_surat character varying(50)
);


ALTER TABLE public.produk OWNER TO postgres;

--
-- Name: produk_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.produk_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.produk_id_seq OWNER TO postgres;

--
-- Name: produk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.produk_id_seq OWNED BY public.produk.id;


--
-- Name: status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.status (
    id integer NOT NULL,
    status character varying(15)
);


ALTER TABLE public.status OWNER TO postgres;

--
-- Name: status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.status_id_seq OWNER TO postgres;

--
-- Name: status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.status_id_seq OWNED BY public.status.id;


--
-- Name: transfer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transfer (
    id integer NOT NULL,
    id_produk integer,
    cabang_asal integer,
    cabang_tujuan integer,
    id_status integer,
    no_surat character varying(50),
    nama_kirim character varying(50),
    nama_terima character varying(50),
    tgl_kirim date,
    tgl_terima date
);


ALTER TABLE public.transfer OWNER TO postgres;

--
-- Name: transfer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transfer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transfer_id_seq OWNER TO postgres;

--
-- Name: transfer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.transfer_id_seq OWNED BY public.transfer.id;


--
-- Name: userfitur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.userfitur (
    id integer NOT NULL,
    id_user integer,
    id_fitur integer
);


ALTER TABLE public.userfitur OWNER TO postgres;

--
-- Name: userfitur_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.userfitur_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.userfitur_id_seq OWNER TO postgres;

--
-- Name: userfitur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.userfitur_id_seq OWNED BY public.userfitur.id;


--
-- Name: voucher; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.voucher (
    id integer NOT NULL,
    kode character varying(50),
    nama character varying(50),
    deskripsi character varying(250),
    nominal integer,
    jumlah integer,
    terpakai integer,
    sisa integer,
    tipe character varying(10),
    tgl_berlaku date,
    tgl_expire date,
    status character varying(2)
);


ALTER TABLE public.voucher OWNER TO postgres;

--
-- Name: voucher_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.voucher_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.voucher_id_seq OWNER TO postgres;

--
-- Name: voucher_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.voucher_id_seq OWNED BY public.voucher.id;


--
-- Name: cabang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cabang ALTER COLUMN id SET DEFAULT nextval('public.cabang_id_seq'::regclass);


--
-- Name: detailkeranjang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detailkeranjang ALTER COLUMN id SET DEFAULT nextval('public.detailkeranjang_id_seq'::regclass);


--
-- Name: fitur id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fitur ALTER COLUMN id SET DEFAULT nextval('public.fitur_id_seq'::regclass);


--
-- Name: fotoproduk id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fotoproduk ALTER COLUMN id SET DEFAULT nextval('public.fotoproduk_id_seq'::regclass);


--
-- Name: jabatan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jabatan ALTER COLUMN id SET DEFAULT nextval('public.jabatan_id_seq'::regclass);


--
-- Name: jenis id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jenis ALTER COLUMN id SET DEFAULT nextval('public.jenis_id_seq'::regclass);


--
-- Name: kategori id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kategori ALTER COLUMN id SET DEFAULT nextval('public.kategori_id_seq'::regclass);


--
-- Name: keranjang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keranjang ALTER COLUMN id SET DEFAULT nextval('public.keranjang_id_seq'::regclass);


--
-- Name: login id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.login ALTER COLUMN id SET DEFAULT nextval('public.login_id_seq'::regclass);


--
-- Name: produk id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.produk ALTER COLUMN id SET DEFAULT nextval('public.produk_id_seq'::regclass);


--
-- Name: status id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.status ALTER COLUMN id SET DEFAULT nextval('public.status_id_seq'::regclass);


--
-- Name: transfer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transfer ALTER COLUMN id SET DEFAULT nextval('public.transfer_id_seq'::regclass);


--
-- Name: userfitur id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.userfitur ALTER COLUMN id SET DEFAULT nextval('public.userfitur_id_seq'::regclass);


--
-- Name: voucher id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.voucher ALTER COLUMN id SET DEFAULT nextval('public.voucher_id_seq'::regclass);


--
-- Data for Name: cabang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cabang (id, nama, alamat, hp, foto, pj, status, persen_produk) FROM stdin;
\.
COPY public.cabang (id, nama, alamat, hp, foto, pj, status, persen_produk) FROM '$$PATH$$/3424.dat';

--
-- Data for Name: detailkeranjang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detailkeranjang (id, id_order, id_cabang, id_produk, harga_cabang, harga_asli, status) FROM stdin;
\.
COPY public.detailkeranjang (id, id_order, id_cabang, id_produk, harga_cabang, harga_asli, status) FROM '$$PATH$$/3426.dat';

--
-- Data for Name: fitur; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fitur (id, fitur, url) FROM stdin;
\.
COPY public.fitur (id, fitur, url) FROM '$$PATH$$/3428.dat';

--
-- Data for Name: fotoproduk; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fotoproduk (id, id_produk, foto) FROM stdin;
\.
COPY public.fotoproduk (id, id_produk, foto) FROM '$$PATH$$/3430.dat';

--
-- Data for Name: jabatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jabatan (id, jabatan) FROM stdin;
\.
COPY public.jabatan (id, jabatan) FROM '$$PATH$$/3432.dat';

--
-- Data for Name: jenis; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jenis (id, jenis) FROM stdin;
\.
COPY public.jenis (id, jenis) FROM '$$PATH$$/3434.dat';

--
-- Data for Name: kategori; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.kategori (id, kategori) FROM stdin;
\.
COPY public.kategori (id, kategori) FROM '$$PATH$$/3436.dat';

--
-- Data for Name: keranjang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.keranjang (id, nama, alamat, id_order, id_cabang, hp, total, status, tgl_transaksi, tgl_bayar, potongan, id_user, tipe_bayar, keterangan, id_voucher) FROM stdin;
\.
COPY public.keranjang (id, nama, alamat, id_order, id_cabang, hp, total, status, tgl_transaksi, tgl_bayar, potongan, id_user, tipe_bayar, keterangan, id_voucher) FROM '$$PATH$$/3438.dat';

--
-- Data for Name: login; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.login (id, nama, username, password, hp, email, foto, token, id_jabatan, status, tgl_buat, tgl_masuk, alamat, nik, id_cabang) FROM stdin;
\.
COPY public.login (id, nama, username, password, hp, email, foto, token, id_jabatan, status, tgl_buat, tgl_masuk, alamat, nik, id_cabang) FROM '$$PATH$$/3440.dat';

--
-- Data for Name: produk; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.produk (id, nama, deskripsi, id_jenis, id_kategori, warna, keterangan, status, tgl_input, tgl_barcode, tgl_laku, barcode, foto, merek, material, dimensi, id_cabang, tgl_display, harga, no_surat) FROM stdin;
\.
COPY public.produk (id, nama, deskripsi, id_jenis, id_kategori, warna, keterangan, status, tgl_input, tgl_barcode, tgl_laku, barcode, foto, merek, material, dimensi, id_cabang, tgl_display, harga, no_surat) FROM '$$PATH$$/3442.dat';

--
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.status (id, status) FROM stdin;
\.
COPY public.status (id, status) FROM '$$PATH$$/3444.dat';

--
-- Data for Name: transfer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.transfer (id, id_produk, cabang_asal, cabang_tujuan, id_status, no_surat, nama_kirim, nama_terima, tgl_kirim, tgl_terima) FROM stdin;
\.
COPY public.transfer (id, id_produk, cabang_asal, cabang_tujuan, id_status, no_surat, nama_kirim, nama_terima, tgl_kirim, tgl_terima) FROM '$$PATH$$/3446.dat';

--
-- Data for Name: userfitur; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.userfitur (id, id_user, id_fitur) FROM stdin;
\.
COPY public.userfitur (id, id_user, id_fitur) FROM '$$PATH$$/3448.dat';

--
-- Data for Name: voucher; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.voucher (id, kode, nama, deskripsi, nominal, jumlah, terpakai, sisa, tipe, tgl_berlaku, tgl_expire, status) FROM stdin;
\.
COPY public.voucher (id, kode, nama, deskripsi, nominal, jumlah, terpakai, sisa, tipe, tgl_berlaku, tgl_expire, status) FROM '$$PATH$$/3450.dat';

--
-- Name: cabang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cabang_id_seq', 2, true);


--
-- Name: detailkeranjang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detailkeranjang_id_seq', 1, false);


--
-- Name: fitur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fitur_id_seq', 9, true);


--
-- Name: fotoproduk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fotoproduk_id_seq', 1, false);


--
-- Name: jabatan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jabatan_id_seq', 11, true);


--
-- Name: jenis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jenis_id_seq', 4, true);


--
-- Name: kategori_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.kategori_id_seq', 4, true);


--
-- Name: keranjang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.keranjang_id_seq', 1, false);


--
-- Name: login_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.login_id_seq', 20, true);


--
-- Name: produk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.produk_id_seq', 6, true);


--
-- Name: status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.status_id_seq', 7, true);


--
-- Name: transfer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transfer_id_seq', 31, true);


--
-- Name: userfitur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.userfitur_id_seq', 24, true);


--
-- Name: voucher_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.voucher_id_seq', 1, false);


--
-- Name: cabang cabang_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cabang
    ADD CONSTRAINT cabang_pkey PRIMARY KEY (id);


--
-- Name: detailkeranjang detailkeranjang_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detailkeranjang
    ADD CONSTRAINT detailkeranjang_pkey PRIMARY KEY (id);


--
-- Name: fitur fitur_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fitur
    ADD CONSTRAINT fitur_pkey PRIMARY KEY (id);


--
-- Name: fotoproduk fotoproduk_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fotoproduk
    ADD CONSTRAINT fotoproduk_pkey PRIMARY KEY (id);


--
-- Name: jabatan jabatan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jabatan
    ADD CONSTRAINT jabatan_pkey PRIMARY KEY (id);


--
-- Name: jenis jenis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jenis
    ADD CONSTRAINT jenis_pkey PRIMARY KEY (id);


--
-- Name: kategori kategori_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (id);


--
-- Name: keranjang keranjang_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.keranjang
    ADD CONSTRAINT keranjang_pkey PRIMARY KEY (id);


--
-- Name: login login_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.login
    ADD CONSTRAINT login_pkey PRIMARY KEY (id);


--
-- Name: login login_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.login
    ADD CONSTRAINT login_username_key UNIQUE (username);


--
-- Name: produk produk_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.produk
    ADD CONSTRAINT produk_pkey PRIMARY KEY (id);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);


--
-- Name: transfer transfer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transfer
    ADD CONSTRAINT transfer_pkey PRIMARY KEY (id);


--
-- Name: userfitur userfitur_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.userfitur
    ADD CONSTRAINT userfitur_pkey PRIMARY KEY (id);


--
-- Name: voucher voucher_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.voucher
    ADD CONSTRAINT voucher_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  